package es.aragon.eapl.client.trace;

public class ConfigurationBean {

	private TipoAuditoria tipoAuditoria;
	private int nivel;
	// 100 (4) se activa el modo trazas	
	// 010 (2) imprime argumentos
	// 001 (1) imprime resultado
	// 000 (0) imprime solamente traza de error
	// (-1) no tracea nada
	// 7 es modo traza e imprime todo, 3 imprime todo, 2 solo argumentos, 1 solo resultado

	public ConfigurationBean() {
		this.tipoAuditoria = TipoAuditoria.DISABLE;
		this.nivel = -1;
	}

	public TipoAuditoria getTipoAuditoria() {
		return tipoAuditoria;
	}

	public void setTipoAuditoria(TipoAuditoria tipoAuditoria) {
		this.tipoAuditoria = tipoAuditoria;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	@Override
	public String toString() {
		return "ConfigurationBean [tipoAuditoria=" + tipoAuditoria + ", nivel="
				+ nivel + "]";
	}
}
