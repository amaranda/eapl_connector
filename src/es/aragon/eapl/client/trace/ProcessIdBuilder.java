package es.aragon.eapl.client.trace;

import java.util.concurrent.atomic.AtomicLong;

public class ProcessIdBuilder {

	public static final String buildNewIdProceso(AtomicLong contador, int threadId) {
		long l1 = System.currentTimeMillis();
		long l2 = System.nanoTime();
		StringBuilder sb = new StringBuilder();
		sb.append(Long.toString(l1, Character.MAX_RADIX)).append('-');
		sb.append(Long.toString(contador.getAndIncrement(), Character.MAX_RADIX)).append('-');
		sb.append(Long.toString(l2, Character.MAX_RADIX)).append('-');
		sb.append(Integer.toString(threadId, Character.MAX_RADIX));
		return sb.toString().toUpperCase();
	}
}
