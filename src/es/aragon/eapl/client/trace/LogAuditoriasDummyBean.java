package es.aragon.eapl.client.trace;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;

/**
 * Sencillo conector para hacer pruebas basicas de configuracion
 * 
 * @author amaranda
 *
 */
public class LogAuditoriasDummyBean {

	public void auditClose(JoinPoint paramJoinPoint, Object retorno) {
		Logger logger = Logger.getLogger(LogAuditoriasDummyBean.class);
		logger.info("auditClose( " + paramJoinPoint + ") > " + retorno);
	}

	public void auditOut(JoinPoint paramJoinPoint, Object retorno) {
		Logger logger = Logger.getLogger(LogAuditoriasDummyBean.class);
		logger.info("auditOut( " + paramJoinPoint + ") > " + retorno);
	}

	public void auditIn(JoinPoint paramJoinPoint) {
		Logger logger = Logger.getLogger(LogAuditoriasDummyBean.class);
		logger.info("auditIn( " + paramJoinPoint + ")");
	}
	
	public void setArchivoPropiedades(String archivoPropiedades) {
		Logger logger = Logger.getLogger(LogAuditoriasDummyBean.class);
		logger.info("archivoPropiedades: " + archivoPropiedades);
	}
}
