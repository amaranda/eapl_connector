package es.aragon.eapl.client.trace;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

public class LogAuditoriasProxy {

	private final Object[] argumentos;
	private final AtomicBoolean primario;
	private final AtomicBoolean closed;
	private final LogAuditoriasBean logAuditoriasBean;

	public LogAuditoriasProxy(Object... argumentos) {
		if( argumentos == null ) {
			this.argumentos = new Object[0];
		}
		else {
			this.argumentos = Arrays.copyOf(argumentos, argumentos.length);
		}
		this.closed = new AtomicBoolean(false);
		this.primario = new AtomicBoolean(false);
		logAuditoriasBean = LogAuditoriasBean.getSingleton();
		if( logAuditoriasBean != null ) {
			logAuditoriasBean.open(argumentos);
		}
	}

	public LogAuditoriasProxy getPrimario() {
		primario.compareAndSet(false, true);
		return this;
	}

	/**
	 * Cierra la traza segun el tipo de la misma (primaria o secundaria)
	 * @param obj
	 * @return
	 */
	public <T> T auditOut(T obj) {
		if( closed.compareAndSet(false, true)  &&  logAuditoriasBean != null ) {
			logAuditoriasBean.auditClose(primario.get(), obj, argumentos);
		}
		return obj;
	}

	/**
	 * Cierra la traza forzando el cierre de la misma y el reinicio de contadores
	 * @param obj
	 * @return
	 */
	public <T> T forceClose(T obj) {
		closed.compareAndSet(false, true);
		if( logAuditoriasBean != null ) {
			logAuditoriasBean.auditClose(true, obj, argumentos);
		}
		return obj;
	}

	public void auditTrace(Object... obj) {
		if( logAuditoriasBean != null ) {
			logAuditoriasBean.auditTrace(obj);
		}
	}

	public void close() {
		if( closed.compareAndSet(false, true)  &&  logAuditoriasBean != null ) {
			logAuditoriasBean.auditClose(primario.get(), new Exception("Unclosed Return LogAuditoriasProxy!!"), argumentos);
		}
	}
}
