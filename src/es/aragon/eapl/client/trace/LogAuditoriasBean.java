package es.aragon.eapl.client.trace;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;

import com.google.gson.Gson;

import es.aragon.eapl.client.services.IAuditar;

public class LogAuditoriasBean {

	private static final AtomicReference<Object> singletonLogAuditoriasBean = new AtomicReference<Object>(null);

	static final LogAuditoriasBean getSingleton() {
		return (LogAuditoriasBean)(singletonLogAuditoriasBean.get());
	}
	private ThreadPoolExecutor colaProceso;
	private PropertiesLoader propertiesLoader;

	private Gson gson;
	private HashSet<String> tssBlackList;
	private HashSet<String> tssWhiteList;

	public LogAuditoriasBean() { }

	private void firstInitLogAuditoriasBean() {
		Logger logger = Logger.getLogger(LogAuditoriasBean.class);
		try {
			this.gson = GsonInitializer.initGson("yyyy-MM-dd'T'HH:mm:ss'Z'");
		}
		catch(Exception e) {
			this.gson = null;
			if( logger != null ) {
				logger.error("LogAuditoriasBean()", e);
			}
		}
		propertiesLoader = new PropertiesLoader();
		tssBlackList = new HashSet<String>();
		tssWhiteList = new HashSet<String>();
		colaProceso = new ThreadPoolExecutor(2, 10, 5, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
	}

	private Logger getLogger() {
		return propertiesLoader.getLogger();
	}

	private Properties p() {
		return propertiesLoader.getProperties();
	}

	private ConfigurationBean configurationBeanDefault() {
		return propertiesLoader.getConfigurationBeanDefault();
	}

	public void setArchivoPropiedades(String archivoPropiedades) {
		long l = System.currentTimeMillis();
		if( singletonLogAuditoriasBean.compareAndSet(null, this) ) {
			firstInitLogAuditoriasBean();
			try {
				propertiesLoader.initProperties(archivoPropiedades);
				String strInfo = "Inicializa app=" + this.p().getProperty("*.app") + "."
				+ Integer.toHexString(singletonLogAuditoriasBean.hashCode())
				+ "(" + Integer.toHexString(this.hashCode()) + ")";
				sendAuditoriaBeanInitAplicacion(this, archivoPropiedades, strInfo, l);
			}
			catch(Exception e) {
				this.gson = null;
				e.printStackTrace();
			}
		}
		else {
			LogAuditoriasBean _logAuditoriasBean = (LogAuditoriasBean)(singletonLogAuditoriasBean.get());
			// copio recursos
			this.colaProceso = _logAuditoriasBean.colaProceso;
			this.tssBlackList = _logAuditoriasBean.tssBlackList;
			this.tssWhiteList = _logAuditoriasBean.tssWhiteList;
			this.propertiesLoader = _logAuditoriasBean.propertiesLoader;
			this.gson = _logAuditoriasBean.gson;
			String strInfo = "Reutilizo app=" + this.p().getProperty("*.app") + "."
			+ Integer.toHexString(singletonLogAuditoriasBean.hashCode())
			+ "(" + Integer.toHexString(_logAuditoriasBean.hashCode()) + " -> " + Integer.toHexString(this.hashCode()) + ")";
			sendAuditoriaBeanInitAplicacion(_logAuditoriasBean, archivoPropiedades, strInfo, l);
		}
	}

	public void auditIn(JoinPoint paramJoinPoint) {
		String servicio = paramJoinPoint.getSignature().getDeclaringTypeName();
		String metodo = paramJoinPoint.getSignature().getName();
		initAuditIn(servicio, metodo, paramJoinPoint.getArgs());
	}

	private void initAuditIn(String servicio, String metodo, Object[] objArgs) {
		long linicio = System.currentTimeMillis();
		Logger debug = getLogger();
		DatosApertura datosApertura = ThreadLocalAuditorias.getOpenMethod();
		if( datosApertura.getHoraInicioTotal() <= 0l ) {
			trazaApertura(debug, datosApertura, linicio, servicio, metodo, objArgs);
			linicio = System.currentTimeMillis();
			datosApertura.setHoraInicioTotal(linicio);
			datosApertura.setServicioApertura(servicio);
			datosApertura.setMetodoApertura(metodo);
			datosApertura.setServicioPrevio(servicio);
			datosApertura.setMetodoPrevio(metodo);
			if( debug != null ) {
				debug.debug("NUEVO HILO:  auditIn() inicializo timers (" + new Date(linicio) + ") para " + servicio + "." + metodo + ", proceso: " + datosApertura.getIdProceso() + ", " + Thread.currentThread());
			}
		}
		else {
			if( debug != null ) {
				debug.debug("SIGUE HILO:  auditIn() inicializo timers (" + new Date(linicio) + ", " + new Date(datosApertura.getHoraInicioTotal()) + ") para " + servicio + "." + metodo + ", proceso: " + datosApertura.getIdProceso() + ", " + Thread.currentThread());
			}
			trazaApertura(debug, datosApertura, linicio, servicio, metodo, objArgs);
		}
		linicio = System.currentTimeMillis();
		datosApertura.setHoraInicioActual(linicio);
		datosApertura.setHoraFinPrevia(linicio);
		datosApertura.setServicioPrevio(servicio);
		datosApertura.setMetodoPrevio(metodo);
	}

	private void trazaApertura(Logger debug, DatosApertura datosApertura, long linicio, String servicio, String metodo, Object[] objArgs) {
		int indexApertura = datosApertura.getIndexApertura().incrementAndGet();
		ConfigurationBean configurationCierre = ConfigurationBeanBuilder.buildConfigurationCierre(getLogger(), configurationBeanDefault(), p(), servicio, metodo);
		if( configurationCierre != null ) {
			try {
				HashMap<String, Object> hm = new HashMap<String, Object>();
				String threadDumpStack = printTrace(servicio, metodo, 128);
				int indexCierre = datosApertura.getIndexCierre().get();
				if( datosApertura.getHoraInicioTotal() <= 0l ) {
					hm.put("apertura", "inicial");
					_auditOut(debug, configurationCierre, servicio, metodo, datosApertura.getIdProceso(), objArgs, hm, threadDumpStack, indexApertura, indexCierre, linicio, linicio);
				}
				else {
					long lInicioTotal = datosApertura.getHoraInicioTotal();
					long lmax = Math.max(datosApertura.getHoraFinPrevia(), datosApertura.getHoraInicioActual());

					hm.put("apertura", "parcial");
					hm.put("fechaPrevia", new Date(lmax));
					hm.put("fechaApertura", new Date(lInicioTotal));
					hm.put("gapApertura", new Long(linicio - lInicioTotal));
					hm.put("servicioPrevio", datosApertura.getServicioPrevio());
					hm.put("metodoPrevio", datosApertura.getMetodoPrevio());
					_auditOut(debug, configurationCierre, servicio, metodo, datosApertura.getIdProceso(), objArgs, hm, threadDumpStack, indexApertura, indexCierre, lmax, linicio);
				}
			} catch (Throwable e) {
				if( debug != null ) {
					debug.debug("resetAll()", e);	
				}
			}
		}
	}

	private String printTrace(String servicio, String metodo, int nivel) {
		StringBuilder sb = new StringBuilder(servicio).append('.').append(metodo).append('\n');
		AtomicInteger ai = new AtomicInteger(Math.max(nivel, 4));
		for( StackTraceElement ste : Thread.currentThread().getStackTrace() ) {
			if( ste.getClassName().startsWith("es.aragon.eapl.client.trace") ){}
			else if( ste.getClassName().startsWith("java.") ){}
			else if( ste.getClassName().startsWith("javax.") ){}
			else {
				if( ai.decrementAndGet() > 0 ) {
					sb.append(ste.toString()).append('\n');
				}
			}
		}
		return sb.toString();
	}

	private void resetAll(Logger debug, Object[] args, long lfin, String servicioCierre, String metodoCierre) {
		try {
			ConfigurationBean configurationCierre = ConfigurationBeanBuilder.buildConfigurationCierre(getLogger(), configurationBeanDefault(), p(), servicioCierre, metodoCierre);
			if( configurationCierre != null ) {
				DatosApertura datosApertura = ThreadLocalAuditorias.getOpenMethod();
				long lInicioTotal = datosApertura.getHoraInicioTotal();
				long lmax = Math.max(datosApertura.getHoraFinPrevia(), datosApertura.getHoraInicioActual());

				String id = datosApertura.getIdProceso();
				if( debug != null ) {
					debug.debug("CIERRO HILO:  resetAll() cierro timers (" + new Date(lmax) + ", " + new Date(lInicioTotal) + ") para " + servicioCierre + "." + metodoCierre + ", proceso: " + id + ", " + Thread.currentThread());
				}
				try {
					HashMap<String, Object> hm = new HashMap<String, Object>();
					hm.put("cierre", new Boolean(true));
					hm.put("fechaPrevia", new Date(lmax));
					hm.put("fechaApertura", new Date(lInicioTotal));
					hm.put("gapApertura", new Long(lmax - lInicioTotal));
					hm.put("duracionCierre", new Long(lfin - lmax));
					hm.put("servicioApertura", datosApertura.getServicioApertura());
					hm.put("metodoApertura", datosApertura.getMetodoApertura());
					int indexApertura = datosApertura.getIndexApertura().incrementAndGet();
					int indexCierre = datosApertura.getIndexCierre().incrementAndGet();
					_auditOut(debug, configurationCierre, servicioCierre, metodoCierre, id, args, hm, null, indexApertura, indexCierre, lmax, lfin);
				} catch (Throwable e) {
					if( debug != null ) {
						debug.debug("resetAll()", e);
					}
				}
			}
		} finally {
			ThreadLocalAuditorias.reset();
			ThreadLocalAuditorias.resetOpenfMethod();
		}
	}

	private void sendAudit(Logger debug, AuditoriaBean auditoriaBean, boolean auditar, boolean tracear) {
		propertiesLoader.check(auditoriaBean.getTimestamp_fin());
		if( auditar  ||  tracear ) {
			if( propertiesLoader.isProxyEnabled() ) {
				ToJsonConversor toJsonConversor = new ToJsonConversor(auditoriaBean.getTimestamp_fin(), auditoriaBean, gson, tssBlackList, tssWhiteList,
						propertiesLoader.getMaximoTamTraza(), propertiesLoader.getMaximaDemora());
				String json = toJsonConversor.build(debug);
				if( json != null ) {
					MR mr = new MR(json, auditar, tracear);
					colaProceso.execute(mr);
				} 
			}
		}
	}

	private class MR implements Runnable {

		final String json;
		final boolean auditar;
		final boolean tracear;

		MR(String j, boolean a, boolean t) {
			json = j;
			auditar = a;
			tracear = t;
		}

		@Override
		public void run() {
			if( auditar  ||  tracear ) {
				try {
					IAuditar iAuditar = propertiesLoader.getProxy();
					if( iAuditar != null ) {
						iAuditar.trace(json, auditar, tracear);	
					}
				} catch (Throwable e) {
					Logger debug = getLogger();
					if( debug != null ) {
						debug.debug("MR()", e);
					}
				}
			}
		}
	}

	public void auditClose(JoinPoint paramJoinPoint, Object retorno) {
		_auditOutClose(paramJoinPoint, retorno, true);
	}

	public void auditOut(JoinPoint paramJoinPoint, Object retorno) {
		_auditOutClose(paramJoinPoint, retorno, false);
	}

	public void _auditOutClose(JoinPoint paramJoinPoint, Object retorno, boolean isClose) {
		long lfin = System.currentTimeMillis();
		Logger debug = getLogger();
		Object[] objArgs = paramJoinPoint.getArgs();
		String servicio = paramJoinPoint.getSignature().getDeclaringTypeName();
		String metodo = paramJoinPoint.getSignature().getName();
		Method method = searchMethodForConfiguration(debug, paramJoinPoint);
		String threadDumpStack = null;
		if( method != null ) {
			servicio = method.getDeclaringClass().getName();
			metodo = method.getName();
			if( method.getReturnType() == Void.TYPE  ||  method.getReturnType() == Void.class ) {
				retorno = Void.TYPE;
			}
		} else {
			if( "true".equalsIgnoreCase(p().getProperty("trace.enabled")) ) {
				threadDumpStack = printTrace(servicio, metodo, 16);
			}
		}
		long lInicio = isClose ? ThreadLocalAuditorias.getOpenMethod().getHoraInicioTotal() : ThreadLocalAuditorias.getOpenMethod().getHoraInicioActual();
		String id = ThreadLocalAuditorias.getOpenMethod().getIdProceso();
		int indexApertura = ThreadLocalAuditorias.getOpenMethod().getIndexApertura().get();
		int indexCierre = ThreadLocalAuditorias.getOpenMethod().getIndexCierre().incrementAndGet();
		if( debug != null ) {
			debug.debug("auditOutClose() finalizo timers (" + new Date(lInicio) + "), proceso: " + id + ", " + Thread.currentThread());
		}
		try {
			ConfigurationBean configurationBean = ConfigurationBeanBuilder.buildConfiguration(getLogger(), configurationBeanDefault(), p(), servicio, metodo);
			_auditOut(debug, configurationBean, servicio, metodo, id, objArgs, retorno, threadDumpStack, indexApertura, indexCierre, lInicio, lfin);
		} catch (Throwable e) {
			if( debug != null ) {
				debug.debug("auditOutClose()", e);
			}
		}
		finally {
			if( isClose ) {
				resetAll(debug, objArgs, lfin, servicio, metodo);	
			}
			else {
				ThreadLocalAuditorias.getOpenMethod().setHoraFinPrevia(lfin);
				ThreadLocalAuditorias.getOpenMethod().setServicioPrevio(servicio);
				ThreadLocalAuditorias.getOpenMethod().setMetodoPrevio(metodo);
			}
		}
		if( debug != null ) {
			debug.debug("auditClose() tiempo empleado " + (System.currentTimeMillis() - lfin) + " msegs");
		}
	}

	private boolean isAuditable(ConfigurationBean c ) {
		return c.getTipoAuditoria() != null  &&  c.getTipoAuditoria().compareTo(TipoAuditoria.TRAZA) < 0;
	}

	private void _auditOut(Logger debug, ConfigurationBean configurationBean, String servicio, String metodo, String idProceso,
			Object[] objArgs, Object retorno, String threadDumpStack, int indexApertura, int indexCierre,
			long linicio, long lfin) throws Throwable {
		if( configurationBean == null ) {}
		else if( isAuditable(configurationBean)  ||  configurationBean.getNivel() >= 0 ) {
			AuditoriaBean auditoriaBean = new AuditoriaBean();
			auditoriaBean.setTipo(configurationBean.getTipoAuditoria());
			auditoriaBean.setUsuario(p().getProperty("*.idUsuario", "NULL"));
			auditoriaBean.setAplicacion(p().getProperty("*.app", "NULL"));
			auditoriaBean.setServicio(servicio);
			auditoriaBean.setMetodo(metodo);
			auditoriaBean.setTimestamp_inicio(linicio);
			auditoriaBean.setTimestamp_fin(lfin);
			auditoriaBean.setDuracion(lfin - linicio);
			auditoriaBean.setIdProceso(idProceso);
			auditoriaBean.setThreadDumpStack(threadDumpStack);
			auditoriaBean.setIndexApertura(indexApertura);
			auditoriaBean.setIndexCierre(indexCierre);
			auditoriaBean.setTipoResultado(null);
			auditoriaBean.setResultado(null);
			if( configurationBean.getNivel() >= 0 ) {
				if( retorno == Void.TYPE  ||  retorno == Void.class ) {
					auditoriaBean.setTipoResultado(Void.TYPE);
				}
				else if( retorno != null ) {
					auditoriaBean.setTipoResultado(retorno.getClass());
				}
				if( objArgs != null ) {
					auditoriaBean.setTipoArgumentos(objArgs);	
				}

				if( configurationBean.getNivel() < 0 ) { } // no se imprime nada de ninguna traza de seguimiento
				else {
					if( retorno instanceof Throwable ) {
						Throwable th = (Throwable)retorno;
						while( th.getCause() != null ) {
							th = th.getCause();
						}
						auditoriaBean.setTipoResultado(th.getClass());
						auditoriaBean.setError(th.toString());
						try( StringWriter sw = new StringWriter() ) {
							try( PrintWriter pw = new PrintWriter(sw) ) {
								pw.println(th.toString());
								((Throwable)retorno).printStackTrace(pw);
							}
							auditoriaBean.setError(sw.getBuffer().toString());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					else if( (configurationBean.getNivel() & 0x001) != 0 ) {
						if( retorno == Void.TYPE  ||  retorno == Void.class ) { }
						else if( retorno != null ) {
							auditoriaBean.setResultado(retorno);
						}
					}
					if( (configurationBean.getNivel() & 0x002) != 0  &&  objArgs != null  &&  objArgs.length > 0 ) {
						auditoriaBean.setArgumentos(objArgs);
						Map<String, Object> hm = ThreadLocalAuditorias.getBean();
						if( !hm.isEmpty() ) {
							HashMap<String, Object> hm2 = new HashMap<String, Object>(hm);
							auditoriaBean.setSesion(hm2);	
						}
					}
				}
			}
			sendAudit(debug, auditoriaBean, isAuditable(configurationBean), configurationBean.getNivel() >= 0 );
		}
	}

	private Method searchMethodForConfiguration(Logger debug, JoinPoint paramJoinPoint) {
		if( debug != null ) {
			debug.debug("searchConfiguration() JoinPoint 1.1 " + paramJoinPoint.getSignature().toShortString());
			debug.debug("searchConfiguration() JoinPoint 1.2 " + paramJoinPoint.getSignature().getDeclaringTypeName());
			debug.debug("searchConfiguration() JoinPoint 1.3 " + paramJoinPoint.getSignature().getName());
			debug.debug("searchConfiguration() JoinPoint 1.4 " + paramJoinPoint.getSignature().toLongString());
		}
		return searchMethodForConfiguration(debug, paramJoinPoint.getSignature().getDeclaringType(), paramJoinPoint.getSignature().getName());
	}

	private Method searchMethodForConfiguration(Logger debug, StackTraceElement stackTraceElement) {
		if( debug != null ) {
			debug.debug("searchConfiguration() 1.1 " + stackTraceElement);
		}
		try {
			Class<?> clase = Thread.currentThread().getContextClassLoader().loadClass(stackTraceElement.getClassName());
			return searchMethodForConfiguration(debug, clase, stackTraceElement.getMethodName());
		} catch (Exception e) {
			if( debug != null ) {
				debug.debug("searchConfiguration() ", e);
			}
		}
		return null;
	}

	private Method searchMethodForConfiguration(Logger debug, Class<?> clase, String metodo) {
		if( clase.getName().startsWith("es.aragon.eapl.client.trace.") ) {
			return null;
		}
		for( Method mm : clase.getMethods() ) {
			if( debug != null ) {
				debug.debug("searchConfiguration() 3 " + mm.toString());
			}
			if( mm.getName().equals(metodo) ) {
				if( debug != null ) {
					debug.debug("searchConfiguration() 4 coinciden!! " + mm.toString());
				}
				return mm;
			}
		}
		return null;
	}

	private StackTraceElement searchStackTraceElement(Logger debug ) {
		String prefx1 = es.aragon.eapl.client.trace.LogAuditoriasProxy.class.getPackage().getName();
		String prefx2 = es.aragon.eapl.trace.LogAuditoriasProxy.class.getPackage().getName();
		boolean encontrado = false;
		for( StackTraceElement ste : Thread.currentThread().getStackTrace() ) {
			if( ste.getClassName().startsWith(prefx1)  ||  ste.getClassName().startsWith(prefx2) ) {
				encontrado = true;
				if( debug != null ) {
					debug.debug("searchStackTraceElement() 1 sigo en " + ste);
				}
			}
			else if( encontrado ) {
				if( debug != null ) {
					debug.debug("searchStackTraceElement() 3 encuentro " + ste);
				}
				return ste;
			}
			else {
				if( debug != null ) {
					debug.debug("searchStackTraceElement() 2 sigo en " + ste);
				}
			}
		}
		return null;
	}

	// accesos singleton
	void open(Object[] objArgs) {
		StackTraceElement stackTraceElement = searchStackTraceElement(getLogger());
		String servicio = stackTraceElement.getClassName();
		String metodo = stackTraceElement.getMethodName();
		initAuditIn(servicio, metodo, objArgs);
	}

	void auditClose(boolean isClose, Object retorno, Object[] argumentos) {
		long lfin = System.currentTimeMillis();
		Logger debug = getLogger(); 
		StackTraceElement stackTraceElement = searchStackTraceElement(debug);
		if( stackTraceElement != null ) {
			String servicio = stackTraceElement.getClassName();
			String metodo = stackTraceElement.getMethodName();
			int indexApertura = ThreadLocalAuditorias.getOpenMethod().getIndexApertura().get();
			int indexCierre = ThreadLocalAuditorias.getOpenMethod().getIndexCierre().incrementAndGet();
			long lInicio = isClose ? ThreadLocalAuditorias.getOpenMethod().getHoraInicioTotal() : ThreadLocalAuditorias.getOpenMethod().getHoraInicioActual();
			String id = ThreadLocalAuditorias.getOpenMethod().getIdProceso();
			Method method = searchMethodForConfiguration(debug, stackTraceElement);
			String threadDumpStack = null;
			if( "true".equalsIgnoreCase(p().getProperty("trace.enabled")) ) {
				threadDumpStack = printTrace(servicio, metodo, 16);
			}
			if( method != null ) {
				servicio = method.getDeclaringClass().getName();
				metodo = method.getName();
				if( method.getReturnType() == Void.TYPE  ||  method.getReturnType() == Void.class ) {
					retorno = Void.TYPE;
				}
			}
			ThreadLocalAuditorias.getOpenMethod().setServicioPrevio(servicio);
			ThreadLocalAuditorias.getOpenMethod().setMetodoPrevio(metodo);
			try {
				ConfigurationBean c = ConfigurationBeanBuilder.buildConfiguration(getLogger(), configurationBeanDefault(), p(), servicio, metodo);
				if( debug != null ) {
					debug.debug("auditClose() interno finalizo timers (" + new Date(lInicio) + "), proceso: " + id + ", " + Thread.currentThread());
				}
				_auditOut(debug, c, servicio, metodo, id, argumentos, retorno, threadDumpStack, indexApertura, indexCierre, lInicio, lfin);
			} catch (Throwable e) {
				if( debug != null ) {
					debug.debug("auditClose()", e);
				}
			}
			finally {
				if( isClose )
					resetAll(debug, argumentos, lfin, servicio, metodo);
				else {
					ThreadLocalAuditorias.getOpenMethod().setHoraFinPrevia(lfin);
					ThreadLocalAuditorias.getOpenMethod().setServicioPrevio(servicio);
					ThreadLocalAuditorias.getOpenMethod().setMetodoPrevio(metodo);
				}
			}
		}
		if( debug != null ) {
			debug.debug("auditClose() tiempo empleado " + (System.currentTimeMillis() - lfin) + " msegs");
		}
	}

	void auditTrace(Object... obj) {
		long lfin = System.currentTimeMillis();
		if( "true".equalsIgnoreCase(p().getProperty("trace.enabled")) ) {
			Logger debug = getLogger();
			StackTraceElement stackTraceElement = searchStackTraceElement(debug);
			if( stackTraceElement != null ) {
				String servicio = stackTraceElement.getClassName();
				String metodo = stackTraceElement.getMethodName();
				ConfigurationBean configurationCierre = ConfigurationBeanBuilder.buildConfigurationCierre(getLogger(), configurationBeanDefault(), p(), servicio, metodo);
				if( configurationCierre != null ) {
					long lInicioTotal = ThreadLocalAuditorias.getOpenMethod().getHoraInicioTotal();
					long lInicioLocal = ThreadLocalAuditorias.getOpenMethod().getHoraInicioActual();
					long lmax = Math.max(ThreadLocalAuditorias.getOpenMethod().getHoraFinPrevia(), ThreadLocalAuditorias.getOpenMethod().getHoraInicioActual());
					String id = ThreadLocalAuditorias.getOpenMethod().getIdProceso();
					int indexApertura = ThreadLocalAuditorias.getOpenMethod().getIndexApertura().get();
					int indexCierre = ThreadLocalAuditorias.getOpenMethod().getIndexCierre().get();
					String threadDumpStack = printTrace(servicio, metodo, 4);
					try {
						if( debug != null ) {
							debug.debug("auditOut() traza para timers (" + new Date(lInicioTotal) + ", " + new Date(lInicioLocal) + "), proceso: " + id + ", " + Thread.currentThread());
						}
						HashMap<String, Object> hm = new HashMap<String, Object>();
						hm.put("apertura", "stackTraceElement");
						hm.put("fechaPrevia", new Date(lmax));
						hm.put("fechaApertura", new Date(lInicioTotal));
						hm.put("gapApertura", new Long(lInicioLocal - lInicioTotal));
						_auditOut(debug, configurationCierre, servicio, metodo, id, obj, hm, threadDumpStack, indexApertura, indexCierre, lInicioLocal, lfin);
					} catch (Throwable e) {
						if( debug != null ) {
							debug.debug("auditClose()", e);
						}
					}
				}
			}
		}
	}

	private static void sendAuditoriaBeanInitAplicacion(LogAuditoriasBean loaAuditoriasBean, String archivoPropiedades, String mensaje, long timestamp_inicio) {
		AuditoriaBean ab = new AuditoriaBean();
		ab.setTipo(TipoAuditoria.INIT);
		long timestamp_fin = System.currentTimeMillis();
		long duracion = timestamp_fin - timestamp_inicio;
		ab.setUsuario(loaAuditoriasBean.p().getProperty("*.idUsuario", "NULL"));
		ab.setAplicacion(loaAuditoriasBean.p().getProperty("*.app", "NULL"));
		ab.setTimestamp_inicio(timestamp_inicio);
		ab.setTimestamp_fin(timestamp_fin);
		ab.setDuracion(duracion);
		ab.setIndexApertura(0);
		ab.setIndexCierre(0);
		ab.setIdProceso(null);
		ab.setServicio(ab.getAplicacion());
		ab.setMetodo("init");
		ab.setArgumentos(new Object[]{archivoPropiedades});
		ab.setTipoArgumentos(new Object[]{".."});
		ab.setTipoResultado(Void.TYPE);
		ab.setResultado(Collections.singletonMap("version", EaplConnectorVersion.VERSION + " >> " + mensaje));
		ab.setSesion(null);
		ab.setError(null);
		ab.setThreadDumpStack(loaAuditoriasBean.printTrace(ab.getAplicacion(), "init", 64));
		Logger debug = Logger.getLogger(LogAuditoriasBean.class);
		loaAuditoriasBean.sendAudit(debug, ab, false, true);
	}
}
