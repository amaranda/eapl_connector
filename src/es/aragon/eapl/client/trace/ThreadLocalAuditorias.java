package es.aragon.eapl.client.trace;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ThreadLocalAuditorias {

	private static final AtomicLong contador = new AtomicLong(1000000000l);

	private static final String buildNewIdProceso() {
		return ProcessIdBuilder.buildNewIdProceso(contador, Thread.currentThread().hashCode());
	}

	private static final ThreadLocal<Map<String, Object>> threadlocalMap = new ThreadLocal<Map<String, Object>>() {
		@Override
		protected Map<String, Object> initialValue() {
			return new HashMap<String, Object>();
		}
	};

	public static final Map<String, Object> getBean() {
		return threadlocalMap.get();
	}

	public static final Object getKey(String key) {
		return threadlocalMap.get().get(key);
	}

	public static final Object put(String key, Object value) {
		if( value == null ) {
			return threadlocalMap.get().remove(key);
		}
		else {
			return threadlocalMap.get().put(key, value);	
		}
	}

	public static final void reset() {
		threadlocalMap.remove();
	}

	private static final ThreadLocal<DatosApertura> threadlocalOpenMethod = new ThreadLocal<DatosApertura>() {
		@Override
		protected DatosApertura initialValue() {
			String idProceso = buildNewIdProceso();
			DatosApertura datosApertura = new DatosApertura(idProceso);
			datosApertura.setServicioApertura(null);
			datosApertura.setMetodoApertura(null);
			datosApertura.setServicioPrevio(null);
			datosApertura.setMetodoPrevio(null);
			datosApertura.setHoraInicioActual(0l);
			datosApertura.setHoraFinPrevia(0l);
			datosApertura.setRunnable(null);
			return datosApertura;
		}
	};

	public static final DatosApertura getOpenMethod() {
		return threadlocalOpenMethod.get();
	}

	public static final void resetOpenfMethod() {
		threadlocalOpenMethod.remove();
	}
}
