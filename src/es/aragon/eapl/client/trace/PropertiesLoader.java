package es.aragon.eapl.client.trace;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;
import org.serest4j.common.TokensLoader;
import org.serest4j.proxy.DirectProxyFactory;

import es.aragon.eapl.client.services.IAuditar;

public class PropertiesLoader {

	private File filePropiedades = null;
	private final AtomicReference<DirectProxyFactory> clientProxyFactory = new AtomicReference<DirectProxyFactory>(null);
	private final AtomicLong al = new AtomicLong(0l);
	private final AtomicBoolean abChequeando = new AtomicBoolean(false);
	private final AtomicLong lastUpdated = new AtomicLong(0l);
	private final AtomicLong lastVersion = new AtomicLong(0l);
	private final ConfigurationBean configurationBeanDefault = new ConfigurationBean();
	private final AtomicReference<Properties> p = new AtomicReference<Properties>(new Properties());
	private final AtomicInteger maximoTamTraza = new AtomicInteger(0);
	private final AtomicLong maximaDemora = new AtomicLong(0l);

	Properties getProperties() {
		return p.get();
	}

	ConfigurationBean getConfigurationBeanDefault() {
		return configurationBeanDefault;
	}

	IAuditar getProxy() {
		DirectProxyFactory cpf = clientProxyFactory.get();
		if( cpf != null ) {
			return cpf.getProxy(IAuditar.class);
		}
		return null;
	}

	int getMaximoTamTraza() {
		return maximoTamTraza.get();
	}

	long getMaximaDemora() {
		return maximaDemora.get();
	}

	boolean isProxyEnabled() {
		return clientProxyFactory.get() != null;
	}

	void check(long timeStamp) {
		if( al.get() < timeStamp ) {
			String codigoApp = p.get().getProperty("*.app");
			if( codigoApp != null ) {
				if( abChequeando.compareAndSet(false, true) ) {
					Logger debug = getLogger(p.get());
					updateTime(debug);
					Thread th = new Thread(new RemotePropertiesLoader(getClass().getClassLoader(), codigoApp));
					th.setPriority(Thread.MIN_PRIORITY);
					th.setDaemon(true);
					th.start();
				}
			}
		}
	}

	public void initProperties(String archivoPropiedades) {
		Logger debug = Logger.getLogger(LogAuditoriasBean.class);
		ClassLoader cl = getClass().getClassLoader();
		if( debug != null ) {
			debug.debug("initProperties() " + cl + " >> " + archivoPropiedades);
		}
		URL url = cl.getResource(archivoPropiedades);
		if( url != null ) {
			try {
				File _filePropiedades = new File(url.getFile());
				if( _filePropiedades.exists()  &&  _filePropiedades.isFile()  &&  _filePropiedades.canRead() ) {
					filePropiedades = new File(_filePropiedades.getAbsolutePath());
				}
			}
			catch (Exception e) {
				if( debug != null ) {
					debug.error("initProperties", e);
				}
			}
		}
		Properties pInit = reloadInitFileProperties(cl, debug);
		p.get().putAll(pInit);
		ConfigurationBean configurationBean = ConfigurationBeanBuilder.buildDefaultConfiguration(getLogger(pInit), p.get(), configurationBeanDefault);
		ConfigurationBeanBuilder.copiarEnDestino(configurationBean, configurationBeanDefault);
		setMaximos(pInit);
		check(System.currentTimeMillis() + 10000l);
	}

	private void setMaximos(Properties p) {
		try{ maximoTamTraza.set(Integer.parseInt(p.get("*.traza.max.size").toString())); }catch(Exception e){maximoTamTraza.set(0);}
		try{ maximaDemora.set(Long.parseLong(p.get("*.traza.max.demora").toString())); }catch(Exception e){maximaDemora.set(0l);}
	}
	
	Logger getLogger() {
		return getLogger(p.get());
	}

	private Logger getLogger(Properties p) {
		if( "true".equals(p.getProperty("*.logger.enabled")) ) {
			Logger logger = Logger.getLogger(LogAuditoriasBean.class);
			if( logger.isDebugEnabled() ) {
				return logger;
			}
		}
		return null;
	}

	private class RemotePropertiesLoader implements Runnable {

		private final String aplicacion;
		private final ClassLoader cl;

		RemotePropertiesLoader(ClassLoader cl, String app) {
			this.aplicacion = app;
			this.cl = cl;
		}
		
		@Override
		public void run() {
			Logger debug = getLogger(p.get());
			try {
				Properties pFinal = _run(debug);
				printProperties(debug, pFinal);
				ConfigurationBean configurationBean = ConfigurationBeanBuilder.buildDefaultConfiguration(getLogger(pFinal), pFinal, configurationBeanDefault);
				ConfigurationBeanBuilder.copiarEnDestino(configurationBean, configurationBeanDefault);
				setMaximos(pFinal);
				p.set(pFinal);
			} catch (Throwable e) {
				if( debug != null ) {
					debug.debug("MR()", e);
				}
			}
			finally {
				abChequeando.set(false);
				updateTime(debug);
				if( debug != null ) {
					debug.debug("nextLoadConfiguration() " + new Date(al.get()));
				}
			}
		}

		private Properties _run(Logger debug) throws Throwable {
			Properties pFinal = new Properties();
			// carga desde fichero original
			if( filePropiedades != null  &&  filePropiedades.lastModified() > lastUpdated.get() ) {
				pFinal.putAll(reloadInitFileProperties(cl, debug));
			}
			else {
				pFinal.putAll(p.get());
			}
			// obtengo los datos desde remoto
			if( isProxyEnabled() ) {
				long actualVersion = lastVersion.get();
				String str = getProxy().configuration(aplicacion, actualVersion);
				if( str != null ) {
					str = str.trim();
					if( str.length() > 0 ) {
						Properties pAux = new Properties();
						try( StringReader sr = new StringReader("\n" + str + "\n\n") ) {
							pAux.load(sr);
						}
						Object strVersion = pAux.remove("es.aragon.eapl.trace.version");
						if( strVersion != null ) {
							try {
								long nuevaVersion = Long.valueOf(strVersion.toString());
								if( nuevaVersion > actualVersion ) {

									pFinal.clear();
									pFinal.putAll(reloadInitFileProperties(cl, debug));
									lastVersion.compareAndSet(lastVersion.get(), nuevaVersion);

									if( pAux.size() > 0 ) {
										for( String key : pAux.stringPropertyNames() ) {
											if( "trace.enabled".equals(key)  ||  key.startsWith("default.") ) {
												String value = pAux.getProperty(key);
												if( value != null  &&  value.trim().length() > 0 ) {
													pFinal.put(key, value);
												}
											}
											else if( key.endsWith(".nivel")  ||  key.endsWith(".tipo") ) {
												String value = pAux.getProperty(key);
												if( value != null  &&  value.trim().length() > 0 ) {
													pFinal.put(key, value.trim());	
												}
												else {
													pFinal.remove(key);
												}
											}
										}
									}
								}
							}
							catch(Exception e){}
						}
					}
				}
			}
			for( String key : new ArrayList<String>(pFinal.stringPropertyNames()) ) {
				String value = pFinal.getProperty(key);
				if( value != null  &&  value.trim().length() > 0 ) {}
				else {
					pFinal.remove(key);
				}
			}
			return pFinal;
		}
	}

	private Properties reloadInitFileProperties(ClassLoader cl, Logger debug) {
		Properties pAux = new Properties();
		lastVersion.set(0l);
		lastUpdated.set(filePropiedades.lastModified());
		try( InputStream inStream = new FileInputStream(filePropiedades) ) {
			pAux.load(inStream);
			Object proxy = pAux.remove("proxy.url");
			if( proxy != null  &&  proxy.toString().trim().length() > 0 ) {
				String proxyToken = pAux.getProperty("proxy.token", "b64:CAcGBQQDAgE=");
				pAux.remove("proxy.token");
				byte[] bToken = TokensLoader.keyToken2Bytes(getClass().getClassLoader(), proxyToken, debug);
				DirectProxyFactory _directProxyFactory = DirectProxyFactory.newInstance(bToken, getLogger(pAux), proxy.toString().trim());
				_directProxyFactory.setTimeout(2000, 2000);
				_directProxyFactory.setSizeToRefresh(50);
				clientProxyFactory.set(_directProxyFactory);
			}
		}
		catch (Exception e) {
			if( debug != null ) {
				debug.error("reloadFileProperties(" + filePropiedades.getPath() + ")", e);
			}
		}
		return pAux;
	}

	private void printProperties(Logger debug, Properties pFinal) {
		try( StringWriter sw = new StringWriter() ) {
			try( PrintWriter out = new PrintWriter(sw) ) {
				out.print("### version actual es.aragon.eapl.trace.version=");
				out.println(lastVersion.get());
				for( String key : new TreeSet<String>(pFinal.stringPropertyNames()) ) {
					String value = pFinal.getProperty(key);
					out.print(key);
					out.print('=');
					out.println(value);
				}
				out.println();
				out.flush();
			}
			sw.flush();
			String strBuff = sw.toString();
			if( debug != null ) {
				debug.debug("printProperties() " + filePropiedades.getAbsolutePath() + " >>\n");
				debug.debug(strBuff);
			}
			File filePropiedadesbak = new File(filePropiedades.getAbsolutePath() + "_bak");
			try( FileWriter fw = new FileWriter(filePropiedadesbak) ) {
				fw.write(strBuff);
				fw.flush();
			}
			catch (Exception e) {
				if( debug != null ) {
					debug.error("updateFileProperties(" + filePropiedadesbak.getPath() + ")", e);
				}
			}
		} catch (IOException e) {}
	}

	private void updateTime(Logger debug) {
		long l = 300000;
		try {
			l = Long.parseLong(p.get().getProperty("*.timer", "300000"));
			l = Math.min(600000l, l);
			l = Math.max(60000l, l);
		} catch (Throwable e) {}
		l = l + (new Random().nextInt(20000) - 10000);
		al.set(System.currentTimeMillis() + l);
	}
}
