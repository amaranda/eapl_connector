package es.aragon.eapl.client.trace;

import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigurationBeanBuilder {

	static ConfigurationBean buildConfigurationCierre(Logger logger, ConfigurationBean cbDefault, Properties p, String servicio, String metodo) {
		if( "true".equalsIgnoreCase(p.getProperty("trace.enabled")) ) {
			ConfigurationBean c = buildConfiguration(logger, cbDefault, p, servicio, metodo);
			if( c.getNivel() > 3 ) {
				c.setTipoAuditoria(TipoAuditoria.TRAZA);
				c.setNivel(7);
				return c;
			}
		}
		return null;
	}

	public static ConfigurationBean buildDefaultConfiguration(Logger logger, Properties p, ConfigurationBean cbDefault) {
		ConfigurationBean c = new ConfigurationBean();
		copiarEnDestino(cbDefault, c);
		StringBuilder sb = new StringBuilder("default.");
		return buildConfiguration(logger, c, p, sb);
	}

	public static ConfigurationBean buildConfiguration(Logger logger, ConfigurationBean cbDefault, Properties p, String servicio, String metodo) {
		StringBuilder sb = new StringBuilder(servicio);
		ConfigurationBean c = new ConfigurationBean();
		copiarEnDestino(cbDefault, c);
		for( int i=sb.length() - 1; i >= 0; i-- ) {
			if( sb.charAt(i) == '.' ) {
				sb.setLength(i + 1);
				c = buildConfiguration(logger, c, p, sb);
				sb.setLength(0);
				sb.append(servicio);
				i = 0;
			}
		}
		sb.append('.');
		c = buildConfiguration(logger, c, p, sb);
		sb.append(metodo).append('.');
		return buildConfiguration(logger, c, p, sb);
	}

	public static void copiarEnDestino(ConfigurationBean origen, ConfigurationBean destino) {
		destino.setTipoAuditoria(origen.getTipoAuditoria());
		destino.setNivel(origen.getNivel());
	}

	private static ConfigurationBean buildConfiguration(Logger logger, ConfigurationBean cbDefault, Properties p, StringBuilder sb) {
		ConfigurationBean c = new ConfigurationBean();
		copiarEnDestino(cbDefault, c);
		int l = sb.length();
		try {
			sb.setLength(l);
			sb.append("tipo");
			String value = p.getProperty(sb.toString());
			if( value != null ) {
				TipoAuditoria ta = TipoAuditoria.valueOf(value.trim().toUpperCase());
				c.setTipoAuditoria(ta);
			}
		}catch(Exception e){} 
		try {
			sb.setLength(l);
			sb.append("nivel");
			String value = p.getProperty(sb.toString());
			if( value != null ) {
				int nivel = Integer.parseInt(value.trim());
				c.setNivel(nivel);
			}
		}catch(Exception e){}
		if( c.getTipoAuditoria().compareTo(TipoAuditoria.TRAZA) >= 0 ) {
			c.setTipoAuditoria(TipoAuditoria.DISABLE);
		}
		sb.setLength(l);
		if( logger != null  &&  logger.isDebugEnabled() ) {
			logger.debug("searchConfiguration() Para " + sb + " >> " + c);	
		}
		return c;
	}
}
