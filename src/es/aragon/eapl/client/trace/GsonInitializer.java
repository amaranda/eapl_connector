package es.aragon.eapl.client.trace;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

public class GsonInitializer {

	public static final Gson initGson(String pattern) {
		GsonBuilder gsb = new GsonBuilder().setDateFormat(pattern).setLongSerializationPolicy(LongSerializationPolicy.STRING);

		ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {

			@Override
			public boolean shouldSkipField(FieldAttributes fieldAttributes) {
				return fieldAttributes.hasModifier(java.lang.reflect.Modifier.STATIC)
						||  fieldAttributes.hasModifier(java.lang.reflect.Modifier.TRANSIENT);
			}

			@Override
			public boolean shouldSkipClass(Class<?> clase) {
				if( clase.isArray()  &&  clase.getComponentType().isPrimitive() ) {
					return true;
				}
				else if( Throwable.class.isAssignableFrom(clase) ) {
					return true;
				}
				return false;
			}

		};
		gsb = gsb.setExclusionStrategies(exclusionStrategy);
		gsb = gsb.disableInnerClassSerialization();

		return gsb.create();
	}
}
