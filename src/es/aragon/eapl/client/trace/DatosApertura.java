package es.aragon.eapl.client.trace;

import java.util.concurrent.atomic.AtomicInteger;

public class DatosApertura {
	
	private final String idProceso;
	private String metodoApertura;
	private String servicioApertura;
	private String metodoPrevio;
	private String servicioPrevio;
	private long horaInicioTotal;
	private long horaInicioActual;
	private long horaFinPrevia;
	private Runnable runnable;
	private final AtomicInteger indexApertura;
	private final AtomicInteger indexCierre;

	DatosApertura(String idProceso) {
		this.idProceso = idProceso;
		this.indexApertura = new AtomicInteger(0);
		this.indexCierre = new AtomicInteger(0);
	}
	public String getIdProceso() {
		return idProceso;
	}
	public String getMetodoApertura() {
		return metodoApertura;
	}
	public void setMetodoApertura(String metodoApertura) {
		this.metodoApertura = metodoApertura;
	}
	public String getServicioApertura() {
		return servicioApertura;
	}
	public void setServicioApertura(String servicioApertura) {
		this.servicioApertura = servicioApertura;
	}
	public String getMetodoPrevio() {
		return metodoPrevio;
	}
	public void setMetodoPrevio(String metodoPrevio) {
		this.metodoPrevio = metodoPrevio;
	}
	public String getServicioPrevio() {
		return servicioPrevio;
	}
	public void setServicioPrevio(String servicioPrevio) {
		this.servicioPrevio = servicioPrevio;
	}
	public long getHoraInicioTotal() {
		return horaInicioTotal;
	}
	public void setHoraInicioTotal(long horaInicioTotal) {
		this.horaInicioTotal = horaInicioTotal;
	}
	public long getHoraInicioActual() {
		return horaInicioActual;
	}
	public void setHoraInicioActual(long horaInicioActual) {
		this.horaInicioActual = horaInicioActual;
	}
	public long getHoraFinPrevia() {
		return horaFinPrevia;
	}
	public void setHoraFinPrevia(long horaFinPrevia) {
		this.horaFinPrevia = horaFinPrevia;
	}
	public AtomicInteger getIndexApertura() {
		return indexApertura;
	}
	public AtomicInteger getIndexCierre() {
		return indexCierre;
	}
	public Runnable getRunnable() {
		return runnable;
	}
	public void setRunnable(Runnable runnable) {
		this.runnable = runnable;
	}
}
