package es.aragon.eapl.client.trace;

public enum TipoAuditoria {

	CREATE, READ, UPDATE, DELETE, TRAZA, INIT, DISABLE 
}
