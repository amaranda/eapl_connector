package es.aragon.eapl.client.trace;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class ToJsonConversor {

	private final TreeMap<Integer, String> hmMap = new TreeMap<Integer, String>();
	private final Set<String> tssBlackList;
	private final Set<String> tssWhiteList;
	private final StringBuilder resultado = new StringBuilder();
	private final AtomicInteger consolidado = new AtomicInteger(0);
	private final AtomicLong timeInicio;
	private final String paqueteBase;
	private final int maximoTamTraza;
	private final long maxDemoraTraza;

	private final AuditoriaBean auditoriaBean;
	private final long tiempoReferencia;
	private final Gson gson;

	public ToJsonConversor(long tiempoReferencia, AuditoriaBean auditoriaBean, Gson gson,  Set<String> tssBlackList,  Set<String> tssWhiteList,
			int maximoTamTraza, long maxDemoraTraza)
	{
		this.tssBlackList = tssBlackList == null ? new TreeSet<String>() : tssBlackList;
		this.tssWhiteList = tssWhiteList == null ? new TreeSet<String>() : tssWhiteList;
		this.auditoriaBean = auditoriaBean;
		this.tiempoReferencia = tiempoReferencia;
		this.gson = gson;
		this.maximoTamTraza = Math.min(1024 * 1024, Math.max(64 * 1024, maximoTamTraza));
		this.maxDemoraTraza = Math.min(1000, Math.max(100, maximoTamTraza));
		this.timeInicio = new AtomicLong(0l);
		this.paqueteBase = auditoriaBean.getServicio() == null ? "" : auditoriaBean.getServicio().toString().trim();
	}

	public String build(Logger logger) {
		try {
			analize(logger);
		} catch (Throwable th) {
			if( logger != null ) {
				logger.error("build auditoria (" + consolidado.get() + ")", th);
			}
		}
		if( logger != null ) {
			logger.debug("build auditoria genera ( " + resultado.length() + " - " + consolidado.get());
		}
		resultado.setLength(consolidado.get());
		while( resultado.charAt(resultado.length() - 1) == ',' ) {
			resultado.deleteCharAt(resultado.length() - 1);
		}
		resultado.append(',').append('"').append("conversionDelay").append('"').append(':');
		resultado.append(gson.toJson(System.currentTimeMillis() - tiempoReferencia));
		resultado.append('}');
		String str = resultado.toString();
		if( logger != null ) {
			logger.debug("build auditoria genera " + str);
		}
		return str;
	}

	private void analize(Logger debug) {
		resultado.setLength(0);
		consolidado.set(0);
		if( auditoriaBean != null ) {
			resultado.append('{');
			analiceToJSon(debug, "aplicacion", auditoriaBean.getAplicacion());
			analiceToJSon(debug, "timestamp_inicio", auditoriaBean.getTimestamp_inicio());
			analiceToJSon(debug, "timestamp_fin", auditoriaBean.getTimestamp_fin());
			analiceToJSon(debug, "duracion", auditoriaBean.getDuracion());
			analiceToJSon(debug, "indexApertura", auditoriaBean.getIndexApertura());
			analiceToJSon(debug, "indexCierre", auditoriaBean.getIndexCierre());

			analiceToJSon(debug, "idProceso", auditoriaBean.getIdProceso());
			analiceToJSon(debug, "metodo", auditoriaBean.getMetodo());
			analiceToJSon(debug, "servicio", auditoriaBean.getServicio());
			analiceToJSon(debug, "error", auditoriaBean.getError());

			analiceToJSon(debug, "usuario", auditoriaBean.getUsuario());
			analiceToJSon(debug, "tipoArgumentos", auditoriaBean.getTipoArgumentos());
			analiceToJSon(debug, "tipoResultado", auditoriaBean.getTipoResultado());

			if( timeInicio.compareAndSet(0l, tiempoReferencia + maxDemoraTraza) ) {
				analiceToJSon(debug, "sesion", auditoriaBean.getSesion());
				analiceToJSon(debug, "argumentos", auditoriaBean.getArgumentos());
				analiceToJSon(debug, "resultado", auditoriaBean.getResultado());

				analiceToJSon(debug, "threadDumpStack", auditoriaBean.getThreadDumpStack());
			}
		}
	}

	private void analiceToJSon(Logger debug, String key, Object value) {
		if( value != null ) {
			consolidado.set(resultado.length());
			if( debug != null ) {
				debug.debug("analiceToJSon para " + key + " - " + consolidado.get());
			}
			resultado.append('"').append(key).append('"').append(':');
			String json = null;
			try {
				json = analize(debug, value, key);
			} catch (Throwable th) {
				json = null;
				if( debug != null ) {
					debug.error("build " + key + " (" + consolidado.get() + ")", th);
				}
				StringBuffer sbError = new StringBuffer();
				int i = 0;
				while( th.getCause() != null ) {
					th = th.getCause();
				}
				sbError.append(th.getMessage()).append('\n');
				for( StackTraceElement ste : th.getStackTrace() ) {
					if( i < 5 ) {
						sbError.append(ste.toString()).append('\n');
					}
					i++;
				}
				json = gson.toJson(Collections.singletonMap("errorConversionJson", sbError.toString()));
			}
			if( json != null ) {
				resultado.append(json);
				consolidado.set(resultado.length());
				resultado.append(',');
			}
			else {
				resultado.setLength(consolidado.get());
			}
		}
	}

	private boolean validarLimites() {
		if( resultado.length() < maximoTamTraza ) {
			if( timeInicio.get() <= 0 ) {
				return true;
			}
			else if( System.currentTimeMillis() <= timeInicio.get() ) {
				return true;
			}
		}
		return false;
	}

	private String analize(Logger debug, Object obj, String nombrePrevio) {
		if( obj != null  &&  validarLimites() ) {
			Integer hashCode = hashCode(obj, nombrePrevio, debug);
			if( hashCode == null ) {
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			if( obj.getClass().isPrimitive()  ||  obj instanceof Date )
				return gson.toJson(obj);
			else if( obj instanceof Boolean )
				return gson.toJson(((Boolean) obj).booleanValue());
			else if( obj instanceof Character ) {
				stringBuilder.setLength(0);
				stringBuilder.append(((Character) obj).charValue());
				return gson.toJson(stringBuilder.toString());
			}
			else if( obj instanceof Enum ) {
				stringBuilder.setLength(0);
				stringBuilder.append('"');
				stringBuilder.append(obj);
				stringBuilder.append('"');
				return stringBuilder.toString();
			}
			else if( obj instanceof Float  ||  obj instanceof Double )
				return gson.toJson(new Double(((Number)obj).doubleValue()));
			else if( obj instanceof BigInteger  ||  obj instanceof BigDecimal )
				return gson.toJson(String.valueOf(obj));
			else if( obj instanceof Number )
				return gson.toJson(new Long(((Number) obj).longValue()));
			else if( obj instanceof CharSequence ) {
				if( ((CharSequence)obj).length() > 0 ) {
					return gson.toJson(obj);
				}
				else {
					return null;
				}
			}
			else if( hmMap.containsKey(hashCode) ) {
				return gson.toJson(Collections.singletonMap("referenceTo", hmMap.get(hashCode)));
			}
			else if( obj.getClass().isArray()  &&  obj.getClass().getComponentType().isPrimitive() ) {
				hmMap.put(hashCode, nombrePrevio);
				int arraySize = Array.getLength(obj);
				if( arraySize > 0 ) {
					if( obj.getClass().getComponentType() == Character.TYPE ) {
						stringBuilder.setLength(0);
						stringBuilder.append(obj);
						if( stringBuilder.length() > 16 * 1024 ) {
							return gson.toJson(stringBuilder.subSequence(0, 256 * 1024).toString() + "... (total:" + stringBuilder.length() + ")");
						}
						else {
							return gson.toJson(stringBuilder.toString());
						}
					}
					else if( obj.getClass().getComponentType().isPrimitive() ) {
						LinkedHashMap<String, Object> lhm = new LinkedHashMap<String, Object>();
						lhm.put("arrayType", obj.getClass().getComponentType().toString());
						lhm.put("arraySize", new Long(arraySize));
						return gson.toJson(lhm);
					}
				}
				else {
					return null;
				}
			}
			else if( obj.getClass().isArray()  ||  obj instanceof Collection ) {
				hmMap.put(hashCode, nombrePrevio);
				if( obj instanceof Collection ) {
					obj = ((Collection<?>) obj).toArray();
				}
				int n = Array.getLength(obj);
				if( n > 0  && validarLimites() ) {
					resultado.append('[');
					int lprevarray = resultado.length(); 
					for( int i=0; i < n; i++ ) {
						if( validarLimites() ) {
							String obRes = analize(debug, Array.get(obj, i), nombrePrevio + "[" + i + "]");
							if( obRes != null ) {
								resultado.append(obRes).append(',');
							}
						}
						else {
							resultado.append(gson.toJson(Collections.singletonMap("next", new Long(i)))).append(',');
							resultado.append(gson.toJson(Collections.singletonMap("resto", new Long(n - i)))).append(',');
							resultado.append(gson.toJson(Collections.singletonMap("size", new Long(n))));
							i = n + 1;
						}
					}
					while( resultado.charAt(resultado.length() - 1) == ',' ) {
						resultado.deleteCharAt(resultado.length() - 1);
					}
					if( lprevarray < resultado.length() ) {
						resultado.append(']');
						return "";
					}
					else {
						resultado.setLength(lprevarray - 1);
						return null;
					}
				}
			}
			else if( obj instanceof Map ) {
				hmMap.put(hashCode, nombrePrevio);
				int ndatos = 0;
				int lsize = resultado.length();
				resultado.append('{');
				for( Object key : ((Map<?, ?>) obj).keySet() ) {
					if( key instanceof String ||  key instanceof Number ) {
						String strK = key.toString();
						Object obj2 = ((Map<?, ?>) obj).get(key);
						if( obj2 != null ) {
							int lsize2 = resultado.length();
							if( validarLimites() ) {
								resultado.append('"').append(strK).append('"').append(':');
								stringBuilder.setLength(0);
								stringBuilder.append(nombrePrevio).append('.').append(strK);
								String strObj = analize(debug, obj2, stringBuilder.toString());
								if( strObj != null ) {
									ndatos++;
									resultado.append(strObj).append(',');
								}
								else {
									resultado.setLength(lsize2);
								}
							}
						}
					}
				}
				if( ndatos > 0 ) {
					if( resultado.charAt(resultado.length() - 1) == ',' ) {
						resultado.deleteCharAt(resultado.length() - 1);
					}
					resultado.append('}');
					return "";
				}
				else {
					resultado.setLength(lsize);
				}
			}
			else if( tssBlackList.contains(obj.getClass().getName()) ) {}
			else if( isCompatibleObject(obj) ) {
				hmMap.put(hashCode, nombrePrevio);
				int nmetodos = 0;
				int lsize = resultado.length();
				resultado.append('{');
				for( Method _m : obj.getClass().getMethods() ) {
					String name = validarGetter(stringBuilder, _m);
					if( name != null ) {
						if( debug != null ) {
							debug.debug("ToJsonConversor (" + nombrePrevio + ") >> " + _m + "!!");
						}
						Class<?>[] _params = _m.getParameterTypes();
						if( _params == null  ||  _params.length <= 0 ) {
							Object obj2 = null;
							try{ obj2 = _m.invoke(obj); }catch(Exception e){ obj2 = null; }
							if( obj2 != null ) {
								int lsize2 = resultado.length();
								if( validarLimites() ) {
									resultado.append('"').append(name).append('"').append(':');
									stringBuilder.setLength(0);
									stringBuilder.append(nombrePrevio).append('.').append(name);
									String strObj = analize(debug, obj2, stringBuilder.toString());
									if( strObj != null ) {
										resultado.append(strObj).append(',');
										nmetodos++;
									}
									else {
										resultado.setLength(lsize2);
									}
								}
							}
						}
					}
				}
				if( nmetodos > 0 ) {
					if( resultado.charAt(resultado.length() - 1) == ',' ) {
						resultado.deleteCharAt(resultado.length() - 1);
					}
					resultado.append('}');
					return "";
				}
				else {
					resultado.setLength(lsize);
				}
			}
			else {
				tssBlackList.add(obj.getClass().getName());
				if( debug != null ) {
					debug.debug("ToJsonConversor (" + nombrePrevio + ") >> " + obj.getClass().getName() + " INCOMPATIBLE con " + paqueteBase + "!!");
				}
			}
		}
		return null;
	}

	private Integer hashCode(Object obj, String nombrePrevio, Logger debug) {
		if( obj != null  &&  nombrePrevio != null  &&  nombrePrevio.trim().length() > 0 ) {
			String objClass = obj.getClass().getName();
			if( !tssBlackList.contains(objClass) ) {
				try{
					return new Integer(obj.hashCode());
				}catch(Throwable th) {
					if( debug != null ) {
						debug.error("Fallo de Conversion a JSon con " + nombrePrevio + ":" + obj.getClass().getName(), th);
					}
					tssBlackList.add(objClass);
				}
			}
		}
		return null;
	}

	private boolean isCompatibleObject(Object object) {
		String strcl = object.getClass().getName();
		if( tssWhiteList.contains(strcl) ) {
			return true;
		}
		else {
			int n = Math.min(strcl.length(), paqueteBase.length());
			int ii = 0;
			for( int i = 0; ii<3 && i<n; i++ ) {
				char c = strcl.charAt(i);
				if( c != paqueteBase.charAt(i) ) return false;
				if( c == '.' ) ii++;
			}
			if( ii > 0 ) {
				tssWhiteList.add(strcl);
				return true;
			}
			else {
				return false;
			}
		}
	}

	private String validarGetter(StringBuilder sb, Method metodo) {
		if( metodo.getDeclaringClass() == Object.class ) {}
		else if( metodo.getDeclaringClass().getName().startsWith("java.") ){}
		else if( metodo.getDeclaringClass().getName().startsWith("javax.") ){}
		else if( Modifier.isStatic(metodo.getModifiers()) ) {}
		else {
			sb.setLength(0);
			sb.append(metodo.getName());
			String name = null;
			if( metodo.getName().startsWith("get") ) {
				char ch = sb.charAt(3);
				sb.setCharAt(3, Character.toLowerCase(ch));
				name = sb.substring(3);
			}
			else if( metodo.getName().startsWith("is") ) {
				char ch = sb.charAt(2);
				sb.setCharAt(2, Character.toLowerCase(ch));
				name = sb.substring(2);
			}
			if( name != null ) {
				for( Field _f : metodo.getDeclaringClass().getDeclaredFields() ) {
					if( _f.getName().equals(name)) {
						if( _f.getDeclaringClass() == Object.class ) {}
						else if( _f.getDeclaringClass().getName().startsWith("java.") ){}
						else if( _f.getDeclaringClass().getName().startsWith("javax.") ){}
						else if( Modifier.isStatic(_f.getModifiers()) ) {}
						else {
							return name;
						}
					}
				}
			}
		}
		return null;
	}
}
