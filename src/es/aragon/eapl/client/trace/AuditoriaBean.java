package es.aragon.eapl.client.trace;

import java.util.LinkedHashMap;
import java.util.Map;

public class AuditoriaBean {

	private TipoAuditoria tipo;
	private String aplicacion;
	private Object usuario; 

	private long timestamp_inicio;
	private long timestamp_fin;
	private long duracion;
	private long indexApertura;
	private long indexCierre;

	private String idProceso;
	private String metodo;
	private String servicio;
	private String tipoArgumentos;
	private Object argumentos;
	private String tipoResultado;
	private Object resultado;
	private Object sesion;
	private String error;
	private String threadDumpStack;

	public Object getTipoArgumentos() {
		return tipoArgumentos;
	}
	public String getTipoResultado() {
		return tipoResultado;
	}
	public String getThreadDumpStack() {
		return threadDumpStack;
	}
	public void setThreadDumpStack(String threadDumpStack) {
		this.threadDumpStack = threadDumpStack;
	}
	public Object getSesion() {
		return sesion;
	}
	public void setSesion(Object sesion) {
		this.sesion = sesion;
	}
	public TipoAuditoria getTipo() {
		return tipo;
	}
	public void setTipo(TipoAuditoria tipo) {
		this.tipo = tipo;
	}
	public String getAplicacion() {
		return aplicacion;
	}
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}
	public Object getUsuario() {
		return usuario;
	}
	public void setUsuario(Object usuario) {
		this.usuario = usuario;
	}
	public long getTimestamp_inicio() {
		return timestamp_inicio;
	}
	public void setTimestamp_inicio(long timestamp_inicio) {
		this.timestamp_inicio = timestamp_inicio;
	}
	public long getTimestamp_fin() {
		return timestamp_fin;
	}
	public void setTimestamp_fin(long timestamp_fin) {
		this.timestamp_fin = timestamp_fin;
	}
	public long getDuracion() {
		return duracion;
	}
	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	public Object getArgumentos() {
		return argumentos;
	}
	public void setArgumentos(Object[] argumentos) {
		if( argumentos != null  &&  argumentos.length > 1 ) {
			Map<Integer, Object> _argumentos = new LinkedHashMap<Integer, Object>(argumentos.length);
			for( int i=0; i<argumentos.length; i++ ) {
				_argumentos.put(new Integer(i), argumentos[i]);
			}
			this.argumentos = _argumentos;
		}
		else if( argumentos != null  &&  argumentos.length > 0 ) {
			this.argumentos = argumentos[0];
		}
		else {
			this.argumentos = null;
		}
	}

	private void fillClassInfo(StringBuilder sb, Class<?> cla) {
		if( cla.isArray() ) {
			sb.append(cla.getComponentType().getName()).append('[').append(']');
		}
		else {
			sb.append(cla.getName());
		}
	}
	
	public void setTipoArgumentos(Object[] argumentos) {
		if( argumentos != null  &&  argumentos.length > 0 ) {
			StringBuilder _tipoArgumentos = new StringBuilder();
			for( int i=0; i<argumentos.length; i++ ) {
				if( argumentos[i] != null ) {
					fillClassInfo(_tipoArgumentos, argumentos[i].getClass());
				}
				else {
					_tipoArgumentos.append("null");
				}
				_tipoArgumentos.append(',');
			}
			if( _tipoArgumentos.length() > 0 ) {
				_tipoArgumentos.deleteCharAt(_tipoArgumentos.length() - 1);
			}
			this.tipoArgumentos = _tipoArgumentos.toString();
		}
		else if( argumentos != null ) {
			this.tipoArgumentos = "void";
		}
		else {
			this.tipoArgumentos = null;
		}
	}
	public Object getResultado() {
		return resultado;
	}
	public void setTipoResultado(Class<?> clase) {
		if( clase == Void.TYPE  ||  clase == Void.class ) {
			this.tipoResultado = "void";
		}
		else if( clase != null ) {
			StringBuilder sb = new StringBuilder();
			fillClassInfo(sb, clase);
			this.tipoResultado = sb.toString();
		}
		else {
			this.tipoResultado = null;
		}
	}
	public void setResultado(Object resultado) {
		this.resultado = resultado;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public long getIndexApertura() {
		return indexApertura;
	}
	public void setIndexApertura(long indexApertura) {
		this.indexApertura = indexApertura;
	}
	public long getIndexCierre() {
		return indexCierre;
	}
	public void setIndexCierre(long indexCierre) {
		this.indexCierre = indexCierre;
	}
}
