package es.aragon.eapl.client.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.serest4j.annotation.db.TMNoDataSource;
import org.serest4j.annotation.endpoint.TMServlet;
import org.serest4j.annotation.service.TMInjectableContext;
import org.serest4j.annotation.service.TMProxyEnabled;
import org.serest4j.context.ContextContainerInyector;
import org.serest4j.context.TMContext;
import org.serest4j.cripto.FlowUtility;

@TMServlet(pattern = "/internal_services")
@TMProxyEnabled(token="f64:k.bin")
@TMInjectableContext
@TMNoDataSource
public class ControllerMaker {

	TMContext contexto;
	Logger debug;
	
	public Iterator<Object> invocacion(String controlador, String classPathRoot, String metodo, Object[] argumentos) throws Exception {
		File rootClass = (classPathRoot == null  ||  classPathRoot.trim().length() <= 0) ? null : new File(classPathRoot);
		ClassLoader cl = getClass().getClassLoader();
		if( debug != null )
			debug.trace("Directorio raiz " + rootClass + " para local LocalFileClassLoader ...");
		if( rootClass != null  &&  rootClass.exists()  &&  rootClass.isDirectory() ) {
			LocalFileClassLoader localFileClassLoader = new LocalFileClassLoader(cl, rootClass);
			cl = localFileClassLoader;
		}
		Class<?> clase = cl.loadClass(controlador);
		invokeMethod(clase, metodo, argumentos);
		return null;
	}

	public Iterator<Object> invocacion(String controlador, Map<String, byte[]> classMap, String metodo, Object[] argumentos) throws Exception {
		MapClassLoader cl = new MapClassLoader(getClass().getClassLoader(), classMap);
		Class<?> clase = cl.loadClass(controlador);
		invokeMethod(clase, metodo, argumentos);
		return null;
	}

	private Object newInstance(Class<?> clase) throws Exception {
		return ContextContainerInyector.propaga(clase.newInstance(), contexto);
	}
	
	private void invokeMethod(Class<?> clase, String metodo, Object[] argumentos) throws Exception {
		Method method = null;
		Object retorno = null;
		if( argumentos != null  &&  argumentos.length > 0 ) {
			for( Method _m : clase.getMethods() ) {
				if( method == null  &&  _m.getName().equals(metodo) ) {
					if( _m.getParameterTypes().length == argumentos.length ) {
						method = _m;
					}
				}
			}
			if( Modifier.isStatic(method.getModifiers()) )
				retorno = method.invoke(null, argumentos);
			else
				retorno = method.invoke(newInstance(clase), argumentos);
			
		}
		else {
			method = clase.getMethod(metodo);
			if( Modifier.isStatic(method.getModifiers()) )
				retorno = method.invoke(null);
			else
				retorno = method.invoke(newInstance(clase));
		}
		if( retorno != null ) {
			contexto.sendOutput(retorno);
		}
	}

	private class MapClassLoader extends ClassLoader {
		
		private final Map<String, byte[]> classMap;

		private MapClassLoader(ClassLoader classLoader, Map<String, byte[]> classMap ) {
			super(classLoader);
			this.classMap = classMap == null ? new HashMap<String, byte[]>() : classMap;
		}

		@Override
		protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException
		{
			Class<?> c = null;
			try {
				c = super.loadClass(name, resolve);
			}catch(Throwable e) {
				c = null;
			}
			if( c == null  &&  classMap.containsKey(name) ) {
				byte[] b = classMap.get(name);
				if( b != null  &&  b.length > 0 ) {
					if( debug != null )
						debug.trace("Cargada " + name + " desde local MapClassLoader ...");
					c = defineClass(name, b, 0, b.length);	
				}
			}
			if( c == null ) {
				throw new ClassNotFoundException("Class " + name + " not found");
			}
			if (resolve) {
				resolveClass(c);
			}
			return c;
		}
	}

	private class LocalFileClassLoader extends ClassLoader {
		
		private final File rootClass;

		private LocalFileClassLoader(ClassLoader classLoader, File rootClass ) {
			super(classLoader);
			this.rootClass = (rootClass != null  &&  rootClass.exists()  &&  rootClass.isDirectory()) ? rootClass.getAbsoluteFile() : null;
		}

		@Override
		protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException
		{
			Class<?> c = null;
			try {
				c = super.loadClass(name, resolve);
			}catch(Throwable e) {
				c = null;
			}
			if( c == null  &&  this.rootClass != null ) {
				String fileName = name.replace('.', File.separatorChar);
				File f = new File(rootClass, fileName + ".class");
				if( debug != null )
					debug.trace("Buscando " + f.getAbsolutePath() + " desde local LocalFileClassLoader ...");
				if( f.exists()  &&  f.isFile() ) {
					ByteArrayOutputStream bout = new ByteArrayOutputStream();
					try( FileInputStream fin = new FileInputStream(f.getAbsoluteFile()) ) {
						FlowUtility.flushStream(fin, bout);
					} catch(Exception e) { bout.reset(); }
					byte[] b = bout.toByteArray();
					if( b != null  &&  b.length > 0 ) {
						if( debug != null )
							debug.trace("Cargada " + name + " desde local LocalFileClassLoader ...");
						c = defineClass(name, b, 0, b.length);	
					}
				}
			}
			if( c == null ) {
				throw new ClassNotFoundException("Class " + name + " not found");
			}
			if (resolve) {
				resolveClass(c);
			}
			return c;
		}
	}
}
