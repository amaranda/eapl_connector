package es.aragon.eapl.client.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import javax.naming.NamingException;
import javax.xml.bind.DatatypeConverter;

import org.serest4j.annotation.db.TMNoDataSource;
import org.serest4j.annotation.endpoint.TMServlet;
import org.serest4j.annotation.service.TMInjectableContext;
import org.serest4j.annotation.service.TMProxyEnabled;
import org.serest4j.context.TMContext;
import org.serest4j.db.InsertAction;
import org.serest4j.db.InsertBuilder;
import org.serest4j.db.QueryAction;
import org.serest4j.db.QueryBuilder;
import org.serest4j.db.TMLobUtilities;
import org.serest4j.db.TMTransactionalLogger;
import org.serest4j.db.TransactionalBaseContainer;

/**
 * Controlador utilizado para accesos a base de datos
 * 
 * Define un punto de acceso por post, por peticiones serializadas en '/eapl_priv/logs_p'.
 * Tambien puede ser accedido desde ILogsController por  '/eapl_priv/logs?' 
 * 
 * @author mario.aranda
 */
@TMServlet(pattern = "/internal_services")
@TMProxyEnabled(token="f64:k.bin")
@TMInjectableContext
@TMNoDataSource
public class DataController {

	TMContext contexto;
	public Iterator<Object> testDS(String enlace) throws IOException {
		return getTabla(enlace, null);
	}

	public Iterator<Object> getTablaCode64(String enlace, String tabla) throws IOException {
		String select = new String(DatatypeConverter.parseBase64Binary(tabla), "UTF-8");
		return getTabla(enlace, select);
	}

	public Iterator<Object> getTabla(String enlace, String tabla) throws IOException {
		if( tabla != null  &&  tabla.trim().length() > 0 ) {
			contexto.sendOutput("Testing " + tabla + " on " + enlace + "\n");
			return testEnlace(enlace, new Object[] { "select * from " + tabla });
		}
		else {
			contexto.sendOutput("Testing " + enlace + "\n");
			return testEnlace(enlace, null);
		}
	}

	public Iterator<Object> testEnlace(String enlace, Object[] array) throws IOException {
		if( array == null  ||  array.length <= 0 ) {
			return testEnlace(enlace, new Object[]{"select 1 from dual"});
		}
		MyTransactionalBaseContainer mtbc = null;
		Throwable thSend = null;
		try {
			mtbc = new MyTransactionalBaseContainer(enlace);
			TMTransactionalLogger tl = mtbc.getTl();
			if( tl == null ) {
				contexto.sendOutput("ERROR: No se ha encontrado el enlace:" + enlace);
				return null;
			}
			try {
				for( Object query : array ) {
					if( query != null ) {
						contexto.sendOutput("\n");
					}
					if( query == null ) {}
					else if( query instanceof String ) {
						_ejecutar(tl, query.toString());
					}
					else if( query instanceof QueryAction ) {
						QueryAction queryAction = (QueryAction)query;
						if( queryAction.getAction() == QueryAction.Action.SELECT ) {
							_ejecutarQueryBuilder(tl,((QueryAction)query).getQb());
						}
						else if( queryAction.getAction() == QueryAction.Action.COUNT ) {
							int n = queryAction.getQb().executeCount(tl);
							contexto.sendOutput("[count(*)]\n" + n + "\n");
						}
						else if( queryAction.getAction() == QueryAction.Action.DELETE ) {
							int n = queryAction.getQb().executeDelete(tl);
							contexto.sendOutput("[Deleted]\n" + n + "\n");
						}
					}
					else if( query instanceof InsertAction ) {
						InsertAction insertAction = (InsertAction)query;
						InsertBuilder ib = insertAction.getIb();
						if( insertAction.getKeys() != null ) {
							int n = ib.executeUpdate(tl, insertAction.getKeys());
							contexto.sendOutput("[Updated]\n" + n + "\n");
							n = insertAction.getKeys().length - 1;
							for( int i=0; i<n; i++ ) {
								contexto.sendOutput("[" + insertAction.getKeys()[i] + "]\t");
							}
							contexto.sendOutput("[" + insertAction.getKeys()[n] + "]\n");
							for( Object obj : ib.getAllKeys() ) {
								Object[] fila = (Object[])obj;
								n = fila.length - 1;
								for( int i=0; i<n; i++ ) {
									String strv = new String(String.valueOf(fila[i]).getBytes("UTF-8"), "UTF-8");
									contexto.getOutput().send(strv).send("\t");
								}
								String strv = new String(String.valueOf(fila[n]).getBytes("UTF-8"), "UTF-8");
								contexto.getOutput().send(strv).send("\n");
							}
						}
						else {
							int n = insertAction.getIb().executeUpdate(tl);
							contexto.sendOutput("[Updated]\n" + n + "\n");
						}
					}
				}
				mtbc.printLog(tl, null);
			}
			catch(Exception e) {
				thSend = e;
				mtbc.printLog(tl, e);
			}
		} catch (Throwable th) {
			thSend = th;
		}
		if( thSend != null ) {
			printThrowable(thSend);
		}
		return null;
	}

	private void printThrowable(Throwable thSend) throws IOException {
		contexto.sendOutput("\n\n");
		StackTraceElement[] arrste = thSend.getStackTrace();
		contexto.sendOutput(thSend.toString());
		for( StackTraceElement ste : arrste ) {
			contexto.sendOutput("    " + ste.toString() + "\n");
		}    
	}

	private void _ejecutar(TMTransactionalLogger tl, String query) throws SQLException, IOException, NamingException {
		try( PreparedStatement ps = tl.getConexion().prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY) ) {
			try( ResultSet rs = ps.executeQuery() ) {
				_processResultSet(rs, tl);
			}
		}
	}

	private void _ejecutarQueryBuilder(TMTransactionalLogger tl, QueryBuilder query) throws SQLException, IOException, NamingException {
		try {
			ResultSet rs = query.executeQuery(tl);
			_processResultSet(rs, tl);
		}
		finally {
			if( query != null ) {
				query.close();
			}
		}
	}

	private void _processResultSet(ResultSet rs, TMTransactionalLogger tl) throws SQLException, IOException {
		if( rs.next() ) {
			int n = rs.getMetaData().getColumnCount();
			tl.println(n + " Columnas disponibles");
			for( int i=1; i<n; i++ ) {
				contexto.sendOutput("[" + rs.getMetaData().getColumnName(i) + "]\t");
			}
			contexto.sendOutput("[" + rs.getMetaData().getColumnName(n) + "]\n");
			long nfilas = 0;
			do {
				for( int i=1; i<n; i++ ) {
					try { TMLobUtilities.loadLobResultSet(tl, rs, contexto.getOutput(), i); }catch(SQLException sqle)
					{ contexto.sendOutput("##" + rs.getMetaData().getColumnName(i) + "##"); tl.println(rs.getMetaData().getColumnName(i)); tl.printThrowable(sqle); tl.flush(); }
					contexto.sendOutput("\t");
				}
				try{ TMLobUtilities.loadLobResultSet(tl, rs, contexto.getOutput(), n); }catch(SQLException sqle)
				{ contexto.sendOutput("##" + rs.getMetaData().getColumnName(n) + "##"); tl.println(rs.getMetaData().getColumnName(n)); tl.printThrowable(sqle); tl.flush(); }
				contexto.sendOutput("\n");
				nfilas++;
				if( nfilas % 10000l == 0l ) {
					tl.println("Leidas " + nfilas + " Filas");
					tl.flush();
				}
			} while( rs.next() );
			tl.println("Leidas " + nfilas + " Filas");
		}
	}

	private class MyTransactionalBaseContainer extends TransactionalBaseContainer {

		MyTransactionalBaseContainer(String dataSource) throws SQLException, NamingException {
			super.initTransaction(dataSource, contexto.getContext().getContextPath(), DataController.class.getSimpleName(), null);
		}

		TMTransactionalLogger getTl() throws SQLException, NamingException {
			return super.initLog("ExecuteQueries");
		}

		public void printLog(TMTransactionalLogger log, Throwable error) {
			super.printLog(log, error);
		}
	}

	public Iterator<Object> getBlob(String dataSource, String tabla, String[] pkColumns, String columnName, Object[] parametros) throws SQLException, NamingException {
		MyTransactionalBaseContainer mtbc = new MyTransactionalBaseContainer(dataSource);
		TMTransactionalLogger tl = mtbc.getTl();
		if( tl != null ) {
			try {
				TMLobUtilities.loadLob(tl, contexto.getOutput(), tabla, pkColumns, columnName, parametros);
				mtbc.printLog(tl, null);
			}
			catch(Exception e) {
				mtbc.printLog(tl, e);
				throw new SQLException(e);
			}
		}
		return null;
	}

	public void updateBlob(String dataSource, String tabla, String[] pkColumns, String columnName, String file, Object[] parametros) throws SQLException, NamingException {
		File ffile = new File(file);
		Throwable th = null;
		if( ffile.exists()  &&  ffile.canRead() ) {
			MyTransactionalBaseContainer mtbc = new MyTransactionalBaseContainer(dataSource);
			TMTransactionalLogger tl = mtbc.getTl();
			if( tl != null ) {
				try( FileInputStream fin = new FileInputStream(ffile.getAbsoluteFile()) ) {
					TMLobUtilities.updateBlob(tl, tabla, pkColumns, columnName, fin, parametros);
					mtbc.printLog(tl, null);
				}
				catch(Exception e) {
					mtbc.printLog(tl, e);
					th = e;
				}
			}
		}
		if( th != null ) {
			throw new SQLException(th);
		}
	}

	public void updateClob(String dataSource, String tabla, String[] pkColumns, String columnName, String file, Object[] parametros) throws SQLException, NamingException {
		File ffile = new File(file);
		Throwable th = null;
		if( ffile.exists()  &&  ffile.canRead() ) {
			MyTransactionalBaseContainer mtbc = new MyTransactionalBaseContainer(dataSource);
			TMTransactionalLogger tl = mtbc.getTl();
			if( tl != null ) {
				try( FileReader fr = new FileReader(file) ) {
					TMLobUtilities.updateClob(tl, tabla, pkColumns, columnName, fr, parametros);
					mtbc.printLog(tl, null);
				}
				catch(Exception e) {
					mtbc.printLog(tl, e);
					th = e;
				}
			}
		}
		if( th != null ) {
			throw new SQLException(th);
		}
	}
}
