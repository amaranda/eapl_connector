package es.aragon.eapl.client.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.serest4j.annotation.db.TMNoDataSource;
import org.serest4j.annotation.endpoint.TMServlet;
import org.serest4j.annotation.service.TMBasicController;
import org.serest4j.annotation.service.TMProxyEnabled;
import org.serest4j.cripto.FlowUtility;
import org.serest4j.cripto.TokenFactory;

/**
 * Controlador utilizado para accesos a ficheros.
 * 
 * Define un punto de acceso por post, por peticiones serializadas en '/eapl_priv/logs_p'.
 * 
 * No puede ser accedido por otros medios 
 * 
 * @author mario.aranda
 */
@TMServlet(pattern = "/internal_services")
@TMProxyEnabled(token="f64:k.bin")
@TMBasicController
@TMNoDataSource
public class FileController {

	private Map<String, File> hm = Collections.synchronizedMap(new HashMap<String, File>());

	public boolean eliminar(String fichero) throws FileNotFoundException, IOException {
		File feliminar = new File(fichero);
		boolean eliminado = false;
		if( feliminar.exists()  &&  !feliminar.isDirectory() ) {
			for( int i=0; i<10  &&  !eliminado; i++ ) {
				eliminado = feliminar.delete();
			}
		}
		return eliminado;
	}

	public long load(String destino, byte[] b) throws IOException {
		File f = new File(destino);
		long l = System.currentTimeMillis();
		if( f.exists()  &&  f.isFile() ) {
			l = f.lastModified();
			if( f.renameTo(new File(f.getAbsolutePath() + ".old")) ) {
				try( FileOutputStream fout = new FileOutputStream(f) ) {
					fout.write(b);
					fout.flush();
				}
				f.setLastModified(l);
			}
			else {
				throw new IOException(f.getAbsolutePath() + " existe!!");
			}
		}
		else {
			try( FileOutputStream fout = new FileOutputStream(f) ) {
				fout.write(b);
				fout.flush();
			}
		}
		if( f.exists() ) {
			return f.length();
		}
		else {
			return -1;
		}
	}

	public String copy(String origen, String destino) throws FileNotFoundException, IOException {
		File fOrigen = new File(origen);
		File fDestino = new File(destino);
		StringBuilder sb = new StringBuilder();
		if( fOrigen.exists()  &&  fOrigen.isFile()  &&  !fDestino.exists() ) {
			try( ByteArrayOutputStream bout = new ByteArrayOutputStream() ) {
				try( FileInputStream fin = new FileInputStream(fOrigen) ) {
					FlowUtility.flushStream(fin, bout);
				}
				sb.append("Leido ").append(fOrigen.getAbsolutePath());
				try( FileOutputStream fout = new FileOutputStream(fDestino) ) {
					fout.write(bout.toByteArray());
					fout.flush();
				}
				sb.append("\nEscrito en ").append(fDestino.getAbsolutePath());
			}
		}
		return sb.toString();
	}

	public String init(String carpeta, String fichero) throws FileNotFoundException, IOException {
		String id = TokenFactory.make();
		File f = new File(carpeta, fichero);
		if( f.exists() ) {
			throw new IOException(f.getAbsolutePath() + " existe!!");
		}
		try( FileOutputStream fout = new FileOutputStream(f) ) {
			if( hm.size() > 1000 ) hm.clear();
			hm.put(id, f);
			fout.write(new byte[0]);
			fout.flush();
			return id;
		}
	}

	public long append(String id, byte[] datos) throws FileNotFoundException, IOException {
		File f = hm.get(id);
		if( f != null  &&  f.exists()  &&  !f.isDirectory() ) {
			if( datos != null  &&  datos.length > 0 ) {
				try( FileOutputStream fout = new FileOutputStream(f, true) ) {
					fout.write(datos);
					fout.flush();
				}
				return f.length();
			}
			else {
				hm.remove(id);	
			}
		}
		return -1;
	}
}
