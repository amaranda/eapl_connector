package es.aragon.eapl.client.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeSet;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.serest4j.annotation.db.TMNoDataSource;
import org.serest4j.annotation.endpoint.TMServlet;
import org.serest4j.annotation.service.TMInjectableContext;
import org.serest4j.annotation.service.TMProxyEnabled;
import org.serest4j.buffers.CacheEstadisticasController;
import org.serest4j.buffers.cloud.CloudCacheRepository;
import org.serest4j.common.ContentType;
import org.serest4j.common.FileLogger;
import org.serest4j.common.PropertiesLoader;
import org.serest4j.context.ControllerStats;
import org.serest4j.context.TMContext;

import es.aragon.eapl.client.trace.EaplConnectorVersion;

/**
 * Controlador utilizado para accesos a logs y a algunas propiedades del sistema
 * 
 * Define un punto de acceso por post, por peticiones serializadas en '/eapl_priv/logs_p'.
 * Tambien puede ser accedido desde ILogsController por  '/eapl_priv/logs?'
 * 
 * @author mario.aranda
 */
@TMServlet(pattern = "/internal_services")
@TMProxyEnabled(token="f64:k.bin")
@TMInjectableContext
@TMNoDataSource
public class LogsController {

	PropertiesLoader propertiesLoader;
	TMContext context;
	Logger debug;

	public String keyTool(String file, String pwd) {
		File tkFile = file == null ? null : new File(file.trim());
		if( tkFile == null  ||  !tkFile.exists() ) {
			tkFile = new File(System.getProperty("java.home"), "lib");
			tkFile = new File(tkFile, "security");
			tkFile = new File(tkFile, "cacerts");
		}
		String password = pwd == null ? "" : pwd.toString();
		
		StringBuffer sb = new StringBuffer("file: " + tkFile.getAbsolutePath() + "\n");
		Provider[] providers = Security.getProviders();
		String[] tipos = new String[]{ "JCEKS", "JKS", "PKCS12" };
		for( Provider provider : providers ) {
			if( provider != null  &&  !provider.isEmpty() ) {
				sb.append("****************************************************************************\n");
				sb.append(" Provider.Name=").append(provider.getName()).append('\n');
				sb.append(" Provider.Info=").append(provider.getInfo()).append('\n');
				for( String key : new TreeSet<String>(provider.stringPropertyNames()) ) {
					sb.append("    Provider.key.").append(key).append('=').append(provider.get(key)).append('\n');
				}
				ArrayList<String> alTypes = new ArrayList<String>();
				for( String type : tipos ) {
					if( provider.containsKey("KeyStore." + type) ) {
						alTypes.add(type);
					}
				}
				for( String type : alTypes ) {
					int l = sb.length();
					try {
						KeyStore ks = KeyStore.getInstance(type, provider);
						try( FileInputStream fis = new FileInputStream(tkFile) ) {
							ks.load(fis, password.toCharArray());
						}
						sb.append("----------------------------------------------------------------------------\n");
						sb.append(" KeyStore.Type=").append(type).append(", Size=").append(ks.size()).append('\n');
						for( Enumeration<String> e = ks.aliases(); e.hasMoreElements(); ) {
							String aliasKey = e.nextElement();
							if( aliasKey != null  &&  ks.containsAlias(aliasKey) ) {
								sb.append("----------------------------------------------------------------------------\n");
								sb.append(" KeyStore.Alias=").append(aliasKey).append(", Creation Date=").append(ks.getCreationDate(aliasKey));
								Certificate certificate = ks.getCertificate(aliasKey);
								sb.append(", Type=").append(certificate.getType()).append('\n');
								sb.append(" KeyStore.Alias.CertificateBase64=").append(DatatypeConverter.printBase64Binary(certificate.getEncoded())).append('\n');
								PublicKey publicKey = certificate.getPublicKey();
								sb.append(" PublicKey.Algorithm=").append(publicKey.getAlgorithm());
								sb.append(", Format=").append(publicKey.getFormat()).append('\n');
								sb.append(" PublicKey.EncodedBase64=").append(DatatypeConverter.printBase64Binary(publicKey.getEncoded())).append('\n');
								sb.append("----------------------------------------------------------------------------\n");
							}
						}
					} catch(Exception e) {
						sb.setLength(l);
						sb.append("\n---- KeyStore.Type=").append(type).append(" Error ").append(e).append('\n');
					}
					sb.append("----------------------------------------------------------------------------\n");
				}
				sb.append("****************************************************************************\n");
			}
		}
		return sb.toString();
	}

	public String localhost() {
		String p = context.getProxyProperty("request.localPort");
		if( "80".equals(p) ) {
			return "http://" + context.getProxyProperty("request.localAddr");
		}
		else {
			return "http://" + context.getProxyProperty("request.localAddr") + ":" + p;
		}
	}

	public String status() {
		return status(false);
	}

	private String toHumanReadable(long l) {
		if( l < (64l * 1024l) ) {
			return Long.toString(l);
		}
		else if( l < (1024l * 1024l) ) {
			l += 512;
			l /= 1024l;
			return Long.toString(l) + " Kb";
		}
		else if( l < (1024l * 1024l * 1024l) ) {
			l += (512 * 1024l);
			l /= 1024l;
			l /= 1024l;
			return Long.toString(l) + " Mb";
		}
		else {
			l += (500 * 1024l * 1024l);
			l /= 1024l;
			l /= 1024l;
			l /= 1000l;
			return Long.toString(l) + " Gb";
		}
	}

	public String status(boolean reset) {
		StringBuffer sb = new StringBuffer();
		sb.append("# Url: ").append(localhost()).append('\n');
		sb.append("# Version: ").append(EaplConnectorVersion.VERSION).append('\n');
		sb.append("# System.currentTimeMillis: ").append(System.currentTimeMillis()).append('\n');
		sb.append("# new Date(): ").append(new Date()).append('\n');
		sb.append("# Available Processors: ").append(Runtime.getRuntime().availableProcessors()).append('\n');
		sb.append("# Max Memory:\t").append(toHumanReadable(Runtime.getRuntime().maxMemory())).append('\n');
		sb.append("# Total Memory:\t").append(toHumanReadable(Runtime.getRuntime().totalMemory())).append('\n');
		sb.append("# Free Memory:\t").append(toHumanReadable(Runtime.getRuntime().freeMemory())).append('\n');
		if( reset ) {
			sb.append("Limpiando memoria para " + context.getContextPath() + "\n\n");
			ControllerStats.resetProcess(context.getContextPath(), new Long(0l).toString());
			ControllerStats.resetCounters(context.getContextPath(), null);
			for( String name : CloudCacheRepository.listCacheNames(context) ) {
				CacheEstadisticasController cec = CloudCacheRepository.getCacheEstadisticasController(context, name);
				sb.append("\n****************************************************************************");
				sb.append("\n* clear " + name + ":");
				sb.append(cec.clearFrom(-1000l));
			}
			Runtime.getRuntime().runFinalization();
			Runtime.getRuntime().gc();
			Runtime.getRuntime().runFinalization();
			Runtime.getRuntime().gc();
			Runtime.getRuntime().runFinalization();
			Runtime.getRuntime().gc();
		}
		sb.append("\n\nEstado de memoria para " + context.getContextPath() + "\n\n");
		ControllerStats.printFactoriaControladores(context.getContextPath(), sb);
		for( String name : CloudCacheRepository.listCacheNames(context) ) {
			CacheEstadisticasController cec = CloudCacheRepository.getCacheEstadisticasController(context, name);
			sb.append("\n****************************************************************************");
			sb.append("\n* ").append(cec.getTipo()).append(" * ").append(cec.getServidores());
			sb.append("\n* " + name + ":");
			sb.append(cec.listar(true));
		}
		return sb.toString();
	}

	public String sysprops() {
		StringBuilder sb = new StringBuilder();
		sb.append("# Url: ").append(localhost()).append('\n');
		sb.append("# Version: ").append(EaplConnectorVersion.VERSION).append('\n');
		sb.append("# System.currentTimeMillis: ").append(System.currentTimeMillis()).append('\n');
		sb.append("# new Date(): ").append(new Date()).append('\n');
		sb.append("# Available Processors: ").append(Runtime.getRuntime().availableProcessors()).append('\n');
		sb.append("# Max Memory:\t").append(toHumanReadable(Runtime.getRuntime().maxMemory())).append('\n');
		sb.append("# Total Memory:\t").append(toHumanReadable(Runtime.getRuntime().totalMemory())).append('\n');
		sb.append("# Free Memory:\t").append(toHumanReadable(Runtime.getRuntime().freeMemory())).append('\n');

		Enumeration<?> e = System.getProperties().propertyNames();
		TreeSet<String> ts = new TreeSet<String>();
		while (e.hasMoreElements()) {
			Object obj = e.nextElement();
			if (obj != null) {
				ts.add(obj.toString());
			}
		}
		for (String key : ts) {
			sb.append(key);
			sb.append('=');
			sb.append(System.getProperty(key));
			sb.append('\n');
		}
		return sb.toString();
	}

	public Iterator<Object> getResource(String name) {
		try( InputStream is = PropertiesLoader.searchInServletContext(context.getContext(), name, debug) ) {
			byte[] b = new byte[1024];
			int n = is.read(b);
			while (n != -1) {
				if (n > 0) {
					context.sendOutput(Arrays.copyOf(b, n));
				}
				n = is.read(b);
			}
		}
		catch(Exception ioe) {
			if( debug != null ) {
				debug.error("getResource(" + name + ")", ioe);
			}
		}
		return null;
	}

	public String checkResource(String name) {
		StringBuilder sb = new StringBuilder();
		try( InputStream is = PropertiesLoader.searchInServletContext(context.getContext(), name, sb) ) {
		}
		catch(Exception ioe) {
			sb.append(ioe);
			if( debug != null ) {
				debug.error("checkResource(" + name + ")", ioe);
			}
		}
		return sb.toString();
	}

	public Iterator<Object> search(long from, String fichero, String cadena) throws IOException {
		File f = new File(fichero);
		if( cadena != null  &&  cadena.trim().length() > 0  &&  f.exists()  &&  !f.isDirectory()  &&  !f.isHidden()  &&  f.canRead()  &&  f.length() >= cadena.trim().length() ) {
			long ncounter = 0;
			int activo = 0;
			try( FileReader fin = new FileReader(f) ) {
				try( LineNumberReader lnr = new LineNumberReader(fin) ) {
					String linea = lnr.readLine();
					while( linea != null ) {
						ncounter += linea.length();
						ncounter++;
						if( from <= 0  ||  ncounter >= from ) {
							if( linea.indexOf(cadena) != -1 ) {
								context.sendOutput("n=").send(ncounter).send(" >> ").send(linea).send('\n');
								activo = 5;
							}
							else if( activo > 0 ) {
								context.sendOutput(linea).send('\n');
								activo--;
							}
						}
						linea = lnr.readLine();
					}
				}
			}
		}
		return null;
	}

	public Iterator<Object> tail(int minleido, String fichero) throws IOException {
		return _tail(minleido, fichero, true);
	}

	public Iterator<Object> tailf(int minleido, String fichero) throws IOException {
		return _tail(minleido, fichero, false);
	}

	private Iterator<Object> _tail(int minleido, String fichero, boolean stop) throws IOException {
		boolean bDesde = false;
		if( minleido < 0 ) {
			bDesde = true;
		}
		final int nmax_bytes = 1024;
		File f = new File(fichero);
		if( f.exists()  &&  !f.isDirectory()  &&  !f.isHidden()  &&  f.canRead() ) {
			long ntotal = f.length();
			if( Math.max(10 * nmax_bytes, minleido) >= ntotal ) {
				try( FileInputStream fin = new FileInputStream(f) ) {
					byte[] b = new byte[nmax_bytes];
					int i = fin.read(b);
					while(i >= 0 ) {
						if (i > 0) {
							context.sendOutput(Arrays.copyOf(b, i));
						}
						i = fin.read(b);
					}
				}
			}
			else {
				long lseek = bDesde ? Math.max(0, Math.abs(minleido)) : Math.max(0, ntotal - Math.abs(minleido));
				try( RandomAccessFile fin = new RandomAccessFile(f, "r"); ) {
					fin.seek(lseek);
					byte[] b = new byte[nmax_bytes];
					if( lseek > 0 ) {
						context.sendOutput((" --- " + ntotal + " caracteres --- " + lseek + " caracteres restantes...\n...").getBytes("UTF-8"));
					}
					long timer = System.currentTimeMillis();
					int i = fin.read(b);
					if( stop ) {
						while( i > 0 ) {
							context.sendOutput(Arrays.copyOf(b, i)).flush();
							i = fin.read(b);
						}
					}
					else {
						context.getOutput().flush();
						while (System.currentTimeMillis() - timer < 30000l) {
							if (i > 0) {
								context.sendOutput(Arrays.copyOf(b, i)).flush();
								timer = System.currentTimeMillis();
							}
							i = fin.read(b);
							if (i <= 0) {
								try { Thread.sleep(2000l); } catch (InterruptedException e) {}
								context.sendOutput("");
							}
						}
					}
					context.getOutput().flush();
				}
			}
		}
		return null;
	}

	public Iterator<Object> ver(String fichero) throws FileNotFoundException, IOException {
		File f = new File(fichero);
		if (f.exists() && !f.isDirectory() && f.canRead()) {
			try (FileInputStream fin = new FileInputStream(f)) {
				byte[] b = new byte[10  * 1024];
				int i = fin.read(b);
				while (i != -1) {
					context.sendOutput(Arrays.copyOf(b, i));
					i = fin.read(b);
				}
			}
		}
		else {
			context.sendOutput( ("No se puede visualizar el fichero: " + fichero).getBytes());
		}
		return null;
	}

	private String validarTipoTexto(String str) {
		String fn = str.toLowerCase();
		if( fn.endsWith(".log")  ||  fn.endsWith(".txt")  ||  fn.endsWith(".properties") )
			return ContentType.TEXT_PLAIN;
		else if( fn.endsWith(".xhtml")  ||  fn.endsWith(".html")  ||  fn.endsWith(".jsp") )
			return ContentType.TEXT_HTML;
		else if( fn.endsWith(".xml") )
			return ContentType.TEXT_HTML;
		else
			return ContentType.APPLICATION_OCTET_STREAM;
	}

	public Iterator<Object> descargar(String fichero, String alias) throws FileNotFoundException, IOException {
		File f = new File(fichero);
		if (f.exists() && !f.isDirectory() && f.canRead()) {
			try (FileInputStream fin = new FileInputStream(f)) {
				int mgsMax = 10000000;
				int max = Math.min(4096, Math.max(1024, fin.available()));
				context.setOutputContent(validarTipoTexto(f.getAbsolutePath()), alias == null ? f.getName() : alias.trim());
				byte[] b = new byte[max];
				int i = fin.read(b);
				ByteArrayOutputStream bout = new ByteArrayOutputStream(2 * max);
				bout.reset();
				while (i != -1) {
					bout.write(b, 0, i);
					if( bout.size() > max ) {
						context.sendOutput(bout.toByteArray());
						bout.reset();
						max *= 2;
						if( max > mgsMax )
							max /= 2;
					}
					i = fin.read(b);
				}
				if( bout.size() > 0 ) {
					context.sendOutput(bout.toByteArray());
				}
				bout.reset();
			}
		}
		else {
			context.sendOutput( ("No se puede visualizar el fichero: " + fichero).getBytes());
		}
		return null;
	}

	public Iterator<Object> listar() throws IOException {
		return _arrFileToString(_listar(FileLogger.getLogsDirectory(context.getRequest().getContextPath())));
	}

	public Iterator<Object> listar(String fichero) throws IOException {
		return _arrFileToString(_listar(fichero));
	}

	private Iterator<Object> _arrFileToString(File[] files) throws IOException {
		if( files != null  &&  files.length > 0 ) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			int i = 0;
			for( File f : files ) {
				String[] fila = new String[5]; // directorio, ruta absoluta, nombre, fecha ultimo acceso, size en bytes
				fila[0] = new Boolean(f.isDirectory()).toString();
				if( f.isDirectory() ) {
					fila[1] = f.getAbsolutePath();
					if( i == 0 ) {
						fila[2] = "../";
					}
					else if( i == 1 ) {
						fila[2] = "./";
					}
					else {
						fila[2] = f.getName();
					}
					fila[3] = f.lastModified() > 0l ? sdf.format(new Date(f.lastModified())) : "   ";
					fila[4] = Long.toString(f.listFiles().length);
					i++;
					context.sendOutput(fila);
				}
			}
			for( File f : files ) {
				String[] fila = new String[5]; // directorio, ruta absoluta, nombre, fecha ultimo acceso, size en bytes
				fila[0] = new Boolean(f.isDirectory()).toString();
				if( !f.isDirectory() ) {
					fila[1] = f.getAbsolutePath();
					fila[2] = f.getName();
					fila[3] = sdf.format(new Date(f.lastModified()));
					fila[4] = Long.toString(f.length());
					context.sendOutput(fila);
				}
			}
		}
		return null;
	}

	private File[] _listar(String fichero) {
		if( debug != null ) {
			debug.debug("Listando ficheros para " + fichero);	
		}
		if (fichero == null) {
			return null;
		}
		File f = new File(fichero);
		if (f.exists()) {
			if( debug != null ) {
				debug.debug("Fichero existente " + f.getAbsolutePath());
			}
			if (f.isDirectory()) {
				return _montar(f.getParentFile(), f, f.listFiles());
			} else {
				return _listar(f.getParentFile().getAbsolutePath());
			}
		}
		return null;
	}

	private File[] _montar(File directorioAnterior, File directorioActual, File[] f) {
		TreeSet<File> ts = new TreeSet<File>(new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				if (o1.isDirectory() && !o2.isDirectory()) {
					return 1;
				} else if (!o1.isDirectory() && o2.isDirectory()) {
					return -1;
				} else if (o1.isDirectory() && o2.isDirectory()) {
					return o1.getAbsolutePath().compareTo(o2.getAbsolutePath());
				} else {
					int i = new Long(o2.lastModified()).compareTo(new Long(o1
							.lastModified()));
					if (i == 0) {
						i = o1.getAbsolutePath()
								.compareTo(o2.getAbsolutePath());
					}
					return i;
				}
			}
		});
		if (f != null) {
			for (File _f : f) {
				if (_f != null && _f.canRead()) {
					ts.add(_f);
				}
			}
		}
		ArrayList<File> al = new ArrayList<File>(ts.size() + 1);
		al.add(directorioAnterior == null ? directorioActual.getAbsoluteFile() : directorioAnterior.getAbsoluteFile());
		al.add(directorioActual.getAbsoluteFile());
		al.addAll(ts);
		return al.toArray(new File[al.size()]);
	}

	public Iterator<Object> eliminar(String fichero) throws FileNotFoundException, IOException {
		File feliminar = new File(fichero);
		boolean eliminado = false;
		if( feliminar.exists()  &&  !feliminar.isDirectory() ) {
			for( int i=0; i<10  &&  !eliminado; i++ ) {
				eliminado = feliminar.delete();
			}
		}
		else if( feliminar.exists()  &&  feliminar.isDirectory() ) {
			for( int i=0; i<10  &&  !eliminado; i++ ) {
				if( feliminar.list().length <= 0 ) {
					eliminado = feliminar.delete();
				}
			}
		}
		return _arrFileToString(_listar(feliminar.getParent()));
	}

	public Iterator<Object> clean(String fichero, String fechaDesde, String fechaHasta, int recursivo, String prefijo, String sufijo) throws IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmm");
		try {
			long ldesde = (fechaDesde == null  ||  fechaDesde.trim().isEmpty()) ?  0l : sdf.parse(fechaDesde.trim()).getTime();
			long lhasta = (fechaHasta == null  ||  fechaHasta.trim().isEmpty()) ?  (System.currentTimeMillis() + 60000l) : sdf.parse(fechaHasta.trim()).getTime();
			File parent = new File(fichero);
			StringBuilder sb = new StringBuilder("Limpiando " + fichero + " desde " + new Date(ldesde));
			String _pref = prefijo == null ? "" : prefijo.trim();
			String _suf = sufijo == null ? "" : sufijo.trim();
			if( _pref.length() <= 0 )
				_pref = null;
			if( _suf.length() <= 0 )
				_suf = null;
			int _recursivo = recursivo <= 0 ? 0 : Math.min(recursivo, 2);
			clean(parent, ldesde, lhasta, _recursivo, _pref, _suf, sb);
			context.sendOutput(sb.toString());
		} catch (ParseException e) {
			throw new IOException(e.getMessage());
		}
		return null;
	}

	private void clean(File parent, long ldesde, long lhasta, int recursivo, String prefijo, String sufijo, StringBuilder sb) throws IOException {
		if( sb.length() > 128 * 1024) {
			context.sendOutput(sb.toString());
			sb.setLength(0);
		}
		if( parent.isDirectory() ) {
			File[] farr = parent.listFiles();
			long llastmofparent = parent.lastModified();
			boolean deleteParent = recursivo > 1  &&  parent.lastModified() >= ldesde  &&  parent.lastModified() <= lhasta;
			if( farr != null  &&  farr.length > 0 ) {
				for( File f : farr ) {
					if( recursivo > 0  ||  f.isFile() ) {
						clean(f.getAbsoluteFile(), ldesde, lhasta, recursivo, prefijo, sufijo, sb);
					}
				}
				try { Thread.sleep(10l); } catch (InterruptedException e) {}
				farr = parent.listFiles();
				try { parent.setLastModified(llastmofparent); } catch (Exception e) {}
			}
			if( farr != null  &&  farr.length > 0 ) {}
			else {
				if( deleteParent ) {
					if( parent.delete()  ||  parent.delete()  ||  parent.delete() ) {
						sb.append("\ndeleted directory: ").append(parent.getAbsolutePath());
					}
					else {
						sb.append("\nFAIL deleted directory: ").append(parent.getAbsolutePath());
					}
				}
			}
		}
		else if( parent.isFile() ) {
			boolean procesar = parent.lastModified() >= ldesde  &&  parent.lastModified() <= lhasta;
			if( procesar  &&  prefijo != null ) {
				procesar = parent.getName().startsWith(prefijo);
			}
			if( procesar  &&  sufijo != null ) {
				procesar = parent.getName().endsWith(sufijo);
			}
			if( procesar ) {
				if( parent.delete()  ||  parent.delete()  ||  parent.delete() ) {
					sb.append("\ndeleted file: ").append(parent.getAbsolutePath());
				}
				else {
					sb.append("\nFAIL deleted file: ").append(parent.getAbsolutePath());
				}
			}
		}
	}

	public String log4j(String file) throws IOException {
		String key = context.getRequest().getContextPath();
		if( "local".equalsIgnoreCase(file) ) {
			Properties p = propertiesLoader.clone();
			FileLogger.remove(key);
			FileLogger fileLogger = new FileLogger(key, p);
			FileLogger.put(key, fileLogger);
			return "local configuration loaded";
		}
		else {
			StringBuilder trace = new StringBuilder();
			try( InputStream is = PropertiesLoader.searchInServletContext(context.getContext(), file.toString().trim(), trace) ) {
				Properties p = new Properties();
				p.load(is);
				FileLogger.remove(key);
				FileLogger fileLogger = new FileLogger(key, p);
				FileLogger.put(key, fileLogger);
			}
			return trace.toString();
		}
	}

	public String filelogger(String properties) throws IOException {
		String key = context.getRequest().getContextPath();
		try( Reader is = new StringReader(properties.trim() + "\n\n") ) { 
			Properties p = new Properties();
			p.load(is);
			FileLogger.remove(key);
			FileLogger fileLogger = new FileLogger(key, p);
			FileLogger.put(key, fileLogger);
		}
		return "Cargadas en " + key + "\n" + properties;
	}

	public Iterator<Object> testDS(String enlace) throws IOException {
		throw new IOException(new IllegalAccessException("Metodo 'testDS(String enlace)' no implementado en " + getClass().getSimpleName()));
	}

	public Iterator<Object> getTablaCode64(String enlace, String tabla) throws IOException {
		throw new IOException(new IllegalAccessException("Metodo 'getTablaC64(String enlace, String tabla)' no implementado en " + getClass().getSimpleName()));
	}

	public String eaplprops() {
		return propertiesLoader.list();
	}

	public String updateEaplProp(String keyPropiedad, String value) {
		return propertiesLoader.putProperty(keyPropiedad, value);
	}
}
