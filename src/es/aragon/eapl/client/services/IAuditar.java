package es.aragon.eapl.client.services;

import org.serest4j.annotation.endpoint.TMLinkController;

@TMLinkController(".eapl.client.services>>.eapl.trace")
public interface IAuditar {

	public void trace(String json, boolean auditar, boolean tracear);

	public String configuration(String app, long version);
}
