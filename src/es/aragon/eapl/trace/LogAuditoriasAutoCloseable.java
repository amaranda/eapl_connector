package es.aragon.eapl.trace;


/**
 * Compatibilidad con las versiones anteriores.
 * 
 * @author amaranda
 *
 */
public class LogAuditoriasAutoCloseable extends LogAuditoriasProxy implements AutoCloseable {

	public LogAuditoriasAutoCloseable(Object... argumentos) {
		super(false, argumentos);
	}
	
	public LogAuditoriasAutoCloseable(boolean primario, Object... argumentos) {
		super(primario, argumentos);
	}
}
