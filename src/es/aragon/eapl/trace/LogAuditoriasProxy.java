package es.aragon.eapl.trace;

/**
 * Compatibilidad con las versiones anteriores.
 * 
 * @author amaranda
 *
 */
public class LogAuditoriasProxy extends es.aragon.eapl.client.trace.LogAuditoriasProxy { 

	public LogAuditoriasProxy(Object... argumentos) {
		this(false, argumentos);
	}

	public LogAuditoriasProxy(boolean primario, Object... argumentos) {
		super(argumentos);
		if( primario )
			getPrimario();
	}
}
